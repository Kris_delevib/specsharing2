'use strict';

module.exports = function() {
  $.gulp.task('watch', function() {
    $.gulp.watch('./source/js/**/*.js', $.gulp.series('scripts'));
    $.gulp.watch('./source/style/**/*.scss', $.gulp.series('stylelint', 'sass', 'display-sass'));
    $.gulp.watch('./source/template/**/*.pug', $.gulp.series('pug'));
    $.gulp.watch('./source/img/**/*.*', $.gulp.series('images'));
  });
};
