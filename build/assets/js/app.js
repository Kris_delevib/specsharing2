$('#test1').datepicker({
  // Можно выбрать тольо даты, идущие за сегодняшним днем, включая сегодня
  minDate: new Date(),
  autoClose: false,
});


$('#test-timepicker').timepicker({
  timeFormat: 'H:mm',
  interval: 30,
  defaultTime: '8',
  dynamic: false,
  scrollbar: true,
});

// //
// // $('.modal').on('scroll', function(e) {
// //   if ($('.datepicker').hasClass('active')) {
// //     $('.datepicker').removeClass('active')
// //   }
// // })
// //
// // // function createElement(tag, props, ...children) {
// // //   const element = document.createElement(tag);
// // //
// // //   if (props) {
// // //     Object.entries(props).forEach(([key, value]) => {
// // //       if (key.startsWith('on') && typeof value === 'function') {
// // //         element.addEventListener(key.substring(2), value);
// // //       } else if (key.startsWith('data-')) {
// // //         element.setAttribute(key, value);
// // //       } else {
// // //         element[key] = value;
// // //       }
// // //     });
// // //   }
// // //
// // //   children.forEach(child => {
// // //     if (Array.isArray(child)) {
// // //       return element.append(...child);
// // //     }
// // //
// // //     if (typeof child === 'string' || typeof child === 'number') {
// // //       child = document.createTextNode(child);
// // //     }
// // //
// // //     if (child instanceof Node) {
// // //       element.appendChild(child);
// // //     }
// // //   });
// // //
// // //   return element;
// // // }
// // //
// // // let dateOne = document.querySelector('#test1');
// // //
// // // if (dateOne) {
// // //   class Calendar {
// // //     constructor(element, obj) {
// // //
// // //       this.parentEl = $('body');
// // //       this.element = $(element);
// // //       this.startDate = moment().startOf('day');
// // //       this.endDate = moment().endOf('day');
// // //
// // //       this.minDate = false;
// // //       this.maxSpan = false;
// // //       this.minYear = moment().clone().subtract(1, 'year').format('YYYY');
// // //       this.maxYear = moment().clone().add(1, 'year').format('YYYY');
// // //       this.leftCalendar = {};
// // //       this.rightCalendar = {};
// // //       this.isShowing = false;
// // //
// // //       // один календарь
// // //       this.singleCalendar = false;
// // //
// // //       if (typeof obj !== 'object' || obj === null) {
// // //         obj = {};
// // //       }
// // //
// // //       this.template = createElement('div', { className: 'calendar' },
// // //         createElement('div', { className: "calendar-inner" },
// // //           createElement('div', { className: 'calendar-drp left' },
// // //             createElement('div', { className: 'calendar-head' },),
// // //             createElement('div', {className: 'calendar-table'}),
// // //           ),
// // //           createElement('div', {className: 'calendar-drp right'},
// // //             createElement('div', {className: 'calendar-head'},
// // //               createElement('div', { className: 'calendar-head-wrap calendar-head-wrap-option'})
// // //             ),
// // //             createElement('div', {
// // //               className: 'calendar-table'
// // //             }),
// // //           ),
// // //         )
// // //       );
// // //
// // //       this.clickDate = this.clickDate.bind(this);
// // //       this.show = this.show.bind(this);
// // //       this.toggle = this.toggle.bind(this);
// // //       this.keydown = this.keydown.bind(this);
// // //       this.outsideClick = this.outsideClick.bind(this);
// // //       this.clickPrev = this.clickPrev.bind(this);
// // //       this.clickNext = this.clickNext.bind(this);
// // //
// // //
// // //       this.locale = {
// // //         daysOfWeek:  moment.localeData('ru').weekdaysMin(),
// // //         monthNames: moment.localeData('ru').months(),
// // //       };
// // //
// // //
// // //       let insert = this.element.parent();
// // //       this.container = $(this.template).appendTo(insert);
// // //
// // //       this.container.find('.calendar-drp')
// // //         .on('mousedown.calendar', 'td.available', this.clickDate)
// // //         .on('click.calendar', '.prev', this.clickPrev)
// // //         .on('click.calendar', '.next ', this.clickNext);
// // //
// // //
// // //       if (typeof obj.singleCalendar === 'boolean') {
// // //         this.singleCalendar = obj.singleCalendar;
// // //
// // //         if (this.singleCalendar) {
// // //           this.endDate = this.startDate.clone()
// // //         }
// // //       }
// // //
// // //       if (this.singleCalendar) {
// // //         this.container.find('.calendar-drp.right').hide();
// // //       }
// // //
// // //       // значение из input
// // //       let start, end;
// // //
// // //       if ($(this.element).is(':text')) {
// // //
// // //         let val = $(this.element).val(),
// // //           split = val.split(' - ');
// // //
// // //         start = end = null;
// // //
// // //         if (split.length == 2) {
// // //           start = moment(split[0], 'DD.MM.YYYY').clone();
// // //           end = 	moment(split[1], 'DD.MM.YYYY').clone()
// // //         } else if (this.singleCalendar && val !== '') {
// // //           start = moment(val, 'DD.MM.YYYY');
// // //           end = moment(val, 'DD.MM.YYYY');
// // //         }
// // //
// // //         if (start !== null && end !== null) {
// // //           this.setStartDate(start);
// // //           this.setEndDate(end);
// // //         }
// // //       }
// // //
// // //       if (this.element.is("input")) {
// // //         this.element.on({
// // //           'click.calendar': this.show,
// // //           'focus.calendar': this.show,
// // //           'keydown.calendar': this.keydown
// // //         })
// // //       } else {
// // //         this.element.on('click.calendar', this.toggle)
// // //         this.element.on('keydown.calendar', this.toggle)
// // //       }
// // //
// // //       this.updateElement();
// // //     }
// // //
// // //     setStartDate(startDate) {
// // //       if (typeof startDate === 'string') {
// // //         this.startDate = moment(startDate, moment.format('DD.MM.YYYY HH:mm'));
// // //       }
// // //
// // //       if (typeof startDate === 'object') {
// // //         this.startDate = moment(startDate);
// // //       }
// // //
// // //       this.updateMonthsInView();
// // //
// // //     }
// // //
// // //     setEndDate (endDate) {
// // //       if (typeof endDate === 'string') {
// // //         this.endDate = moment(startDate, moment.format('DD.MM.YYYY HH:mm'));
// // //       }
// // //
// // //       if (typeof endDate === 'object') {
// // //         this.endDate = moment(endDate);
// // //       }
// // //
// // //       if (this.endDate.isBefore(this.startDate)) {
// // //         this.endDate = this.startDate.clone();
// // //       }
// // //
// // //       if (this.maxSpan && this.startDate.clone().add(this.maxSpan).isBefore(this.endDate)) {
// // //         this.endDate = this.startDate.clone().add(this.maxSpan);
// // //       }
// // //
// // //       this.updateMonthsInView();
// // //     }
// // //
// // //     isInvalidDate() {
// // //       return false;
// // //     }
// // //
// // //     isCustomDate() {
// // //       return false;
// // //     }
// // //
// // //     updateView() {
// // //       this.updateMonthsInView();
// // //       this.updateCalendars();
// // //     }
// // //
// // //     updateMonthsInView() {
// // //       if (this.endDate) {
// // //         this.leftCalendar.month = this.startDate.clone().date(2);
// // //
// // //         if (this.endDate.month() != this.startDate.month() || this.endDate.year() != this.startDate.year()) {
// // //           this.rightCalendar.month = this.endDate.clone().date(2);
// // //         } else {
// // //           this.rightCalendar.month = this.startDate.clone().date(2).add(1, 'month');
// // //         }
// // //       }
// // //     }
// // //
// // //     updateCalendars() {
// // //       this.renderCalendar('left');
// // //       this.renderCalendar('right');
// // //     }
// // //
// // //     renderCalendar(side) {
// // //       var calendar = side == 'left' ? this.leftCalendar : this.rightCalendar;
// // //
// // //       var month = calendar.month.month();
// // //       var year = calendar.month.year();
// // //
// // //       var daysInMonth = moment([year, month]).daysInMonth();
// // //       var firstDay = moment([year, month, 1]);
// // //       var lastDay = moment([year, month, daysInMonth]);
// // //       var lastMonth = moment(firstDay).subtract(1, 'month').month();
// // //       var lastYear = moment(firstDay).subtract(1, 'month').year();
// // //       var daysInLastMonth = moment([lastYear, lastMonth]).daysInMonth();
// // //       var dayOfWeek = firstDay.day();
// // //
// // //       var hour = calendar.month.hour();
// // //       var minute = calendar.month.minute();
// // //
// // //       var calendar = [];
// // //       calendar.firstDay = firstDay;
// // //       calendar.lastDay = lastDay;
// // //
// // //       for (var i = 0; i < 6; i++) {
// // //         calendar[i] = [];
// // //       }
// // //
// // //       var startDay = daysInLastMonth - dayOfWeek + moment.localeData('ru').firstDayOfWeek() + 1;
// // //       if (startDay > daysInLastMonth)
// // //         startDay -= 7;
// // //
// // //       if (dayOfWeek == moment.localeData('ru').firstDayOfWeek())
// // //         startDay = daysInLastMonth - 6;
// // //
// // //       var curDate = moment([lastYear, lastMonth, startDay, 12, minute]);
// // //
// // //       var col, row;
// // //       for (var i = 0, col = 0, row = 0; i < 42; i++, col++, curDate = moment(curDate).add(24, 'hour')) {
// // //         if (i > 0 && col % 7 === 0) {
// // //           col = 0;
// // //           row++;
// // //         }
// // //         calendar[row][col] = curDate.clone().hour(hour).minute(minute);
// // //         curDate.hour(12);
// // //       }
// // //
// // //       // make the calendar object available to hoverDate/clickDate
// // //       if (side == 'left') {
// // //         this.leftCalendar.calendar = calendar;
// // //       } else {
// // //         this.rightCalendar.calendar = calendar;
// // //       }
// // //
// // //       //выбор времени и даты
// // //       var currentMonth = calendar[1][1].month();
// // //       console.log(currentMonth)
// // //       var currentYear = calendar[1][1].year();
// // //
// // //       let minDate = side == 'left' ?false:this.startDate;
// // //
// // //       var maxYear = this.maxYear;
// // //       var minYear = (minDate && minDate.year()) || (this.minYear);
// // //       var inMinYear = currentYear == minYear;
// // //       var inMaxYear = currentYear == maxYear;
// // //
// // //
// // //
// // //
// // //       var nameMonth = moment.localeData('ru').months();
// // //
// // //       var monthHtml = "<div class='calendar-head-wrap'>";
// // //       monthHtml += '<button class="prev available"><span><</span></button>';
// // //
// // //       var dateHtml = this.locale.monthNames[calendar[1][1].month()] + calendar[1][1].format(" YYYY");
// // //
// // //       monthHtml += '<div class="calendar-caption">' + dateHtml + '</div>';
// // //
// // //       monthHtml += '<button class="next available"><span>></span></button>';
// // //
// // //       this.container.find('.calendar-drp.' + side + ' .calendar-head').html(monthHtml);
// // //
// // //       var html = '<table>';
// // //       html += '<thead>';
// // //       html += '<tr>';
// // //       $.each(this.locale.daysOfWeek, function(index, dayOfWeek) {
// // //         html += '<th>' + dayOfWeek + '</th>';
// // //       });
// // //       html += '<tr>';
// // //       html += '</thead>';
// // //       html += '<tbody>';
// // //
// // //       for (let row = 0; row < 6; row++) {
// // //         html += '<tr>';
// // //
// // //         // add week number
// // //         for (let col = 0; col < 7; col++) {
// // //           let classes = [];
// // //
// // //           // активный
// // //           if (calendar[row][col].format('DD.MM.YYYY') == this.startDate.format('DD.MM.YYYY')) {
// // //             classes.push('active', 'start-date');
// // //           }
// // //
// // //           //today
// // //           if (calendar[row][col].isSame(new Date(), "day"))
// // //             classes.push('today');
// // //
// // //           //highlight the currently selected end date
// // //           if (this.endDate != null && calendar[row][col].format('DD.MM.YYYY') == this.endDate.format('DD.MM.YYYY'))
// // //             classes.push('active', 'end-date');
// // //
// // //
// // //           // weekends
// // //           if (calendar[row][col].isoWeekday() > 5)
// // //             classes.push('weekend');
// // //
// // //           //highlight dates in-between the selected dates
// // //           if (this.endDate != null && calendar[row][col] > this.startDate && calendar[row][col] < this.endDate)
// // //             classes.push('in-range');
// // //
// // //
// // //           //grey out the dates in other months displayed at beginning and end of this calendar
// // //           if (calendar[row][col].month() != calendar[1][1].month())
// // //             classes.push('off', 'ends');
// // //
// // //           //don't allow selection of date if a custom function decides it's invalid
// // //           if (this.isInvalidDate(calendar[row][col]))
// // //             classes.push('off', 'disabled');
// // //
// // //
// // //           //apply custom classes for this date
// // //           var isCustom = this.isCustomDate(calendar[row][col]);
// // //           if (isCustom !== false) {
// // //             if (typeof isCustom === 'string')
// // //               classes.push(isCustom);
// // //             else
// // //               Array.prototype.push.apply(classes, isCustom);
// // //           }
// // //
// // //
// // //           var cname = '', disabled = false;
// // //           for (var i = 0; i < classes.length; i++) {
// // //             cname += classes[i] + ' ';
// // //             if (classes[i] == 'disabled')
// // //               disabled = true;
// // //           }
// // //           if (!disabled)
// // //             cname += 'available';
// // //           html += '<td class="' + cname.replace(/^\s+|\s+$/g, '') + '" data-title="' + 'r' + row + 'c' + col + '">' + calendar[row][col].date() + '</td>';
// // //         }
// // //         html += '</tr>';
// // //       }
// // //       html += '</tbody>';
// // //       html += '</table>';
// // //
// // //       this.container.find('.calendar-drp.' + side + ' .calendar-table').html(html);
// // //     }
// // //
// // //     move() {
// // //       this.container.css({
// // //         top: 80,
// // //         left: 0,
// // //         right: 'auto',
// // //         visibility: 'visible',
// // //         opacity: 1
// // //       });
// // //     }
// // //
// // //     show() {
// // //       if (this.isShowing) return;
// // //
// // //       $(document)
// // //         .on('mousedown', this.outsideClick)
// // //         //mob
// // //         .on('touchend', this.outsideClick)
// // //         .on('focusin', this.outsideClick);
// // //
// // //       this.updateView();
// // //       this.container.show();
// // //       this.move();
// // //       this.element.trigger('show', this);
// // //       this.isShowing = true;
// // //     }
// // //
// // //     hide() {
// // //       if (!this.isShowing) return;
// // //
// // //       if (!this.endDate) {
// // //         this.startDate = this.oldStartDate.clone();
// // //         this.endDate = this.oldEndDate.clone();
// // //       }
// // //
// // //       this.updateElement();
// // //
// // //       $(document).off('.calendar');
// // //       $(window).off('.calendar');
// // //       this.container.hide();
// // //       this.element.trigger('hide.calendar', this);
// // //       this.isShowing = false;
// // //     }
// // //
// // //     outsideClick(e) {
// // //       let target = $(e.target);
// // //
// // //       if (target.closest(this.element).length || target.closest(this.container).length || target.closest('.calendar-inner').length) return
// // //       this.hide();
// // //     }
// // //
// // //     toggle() {
// // //       if (this.isShowing) {
// // //         this.hide();
// // //       } else {
// // //         this.show();
// // //       }
// // //     }
// // //
// // //     clickPrev() {
// // //       this.leftCalendar.month.subtract(1, 'month');
// // //       this.updateCalendars();
// // //     }
// // //
// // //     clickNext() {
// // //       this.leftCalendar.month.add(1, 'month');
// // //       this.updateCalendars();
// // //     }
// // //
// // //     clickDate(e) {
// // //       let title = $(e.target).attr('data-title');
// // //       let row = title.substr(1, 1);
// // //       let col = title.substr(3, 1);
// // //       let cal = $(e.target).parents('.calendar-drp');
// // //       let date = cal.hasClass('left') ? this.leftCalendar.calendar[row][col] : this.rightCalendar.calendar[row][col];
// // //
// // //       if (this.endDate || date.isBefore(this.startDate, 'day')) {
// // //         this.endDate = null;
// // //         this.setStartDate(date.clone());
// // //       }
// // //
// // //       if (this.singleCalendar) {
// // //         this.setEndDate(this.startDate);
// // //       }
// // //
// // //       this.updateView();
// // //       e.stopPropagation();
// // //     }
// // //
// // //     keydown(e) {
// // //       // 9 - tab, 27 - esc, 27 - enter
// // //       if ((e.keyCode === 9) || (e.keyCode === 13)) {
// // //         this.hide();
// // //       }
// // //
// // //       if (e.keyCode === 27) {
// // //         e.preventDefault();
// // //         e.stopPropagation();
// // //         this.hide();
// // //       }
// // //     }
// // //
// // //     updateElement() {
// // //       if (this.element.is('input')) {
// // //         let newValue = this.startDate.format('DD.MM.YYYY HH:mm');
// // //
// // //         if (newValue !== this.element.val()) {
// // //           this.element.val(newValue).trigger('change');
// // //         }
// // //       }
// // //     }
// // //
// // //     remove() {
// // //       this.container.remove();
// // //     }
// // //   }
// // //   new Calendar(dateOne, {
// // //     singleCalendar: true
// // //   });
// // // }




(function () {
  let fileInput = document.querySelectorAll('.input-file');

  Array.from(fileInput).forEach(item => {
    item.addEventListener('change', function (e) {
      let _this = this.previousElementSibling;

      var value = e.target.value.length > 0 ? e.target.value : _this.textContent;
      _this.textContent = value.replace('C:\\fakepath\\', '');
    })
  });

  $('.result-block').click(function() {
    console.log('234');
    let m = new Error('I was constructed via the "new" keyword!');
    $.notify(m, {
      clickToHide: false,
      autoHideDelay: 100000,
    });
  });

  // высота
  let bodyAddHeigth = document.querySelector('.chat-block__body');

  // let headerMessageHead = document.querySelector('.chat-block__right').getBoundingClientRect().height;
  let footerMessageHeight = document.querySelector('.message-input').getBoundingClientRect().height;

  // фотки для сообщений
  var fileView = document.querySelector(".select-choose");
  var fileListDisplay = document.querySelector(".chat-view");
  var fileList = [];

  if (fileView) {
    fileView.addEventListener('change', function () {
      fileList = [];

      for (var i = 0; i < fileView.files.length; i++) {
        fileList.push(fileView.files[i]);
      }

      fileList.forEach(function (file) {
        var fileDisplayEl = document.createElement("div");
        fileDisplayEl.classList.add('file-view');

        fileDisplayEl.style.backgroundImage = "url('assets/img/result.png')";

        fileDisplayEl.innerHTML = `
					<button class="del-vew"><img src="assets/img/del-view.png" alt="" /></button>
					<div>. ${file.name.split('.').pop()}<div>
	    	`;
        fileListDisplay.appendChild(fileDisplayEl);
      });

      if(document.documentElement.clientWidth <= 1200) {
        let heightElementAdd = fileListDisplay.getBoundingClientRect().height;
        if (fileListDisplay.hasChildNodes()) {
          // bodyAddHeigth.style.height = "calc("+100+"vh - ("+footerMessageHeight + "px + "+headerMessageHead+"px + "+heightElementAdd+"px + "+15+"px))";
        }
      }
    });
  }


  document.addEventListener('click', (e) => {
    let target = e.target;

    let el = target.closest('.chat-view .del-vew');
    if (!el) return;

    console.log(el.parentElement.parentElement)

    el.parentNode.remove();

    if (!fileListDisplay.hasChildNodes()) {
      bodyAddHeigth.style.height = "calc("+100+"vh - ("+footerMessageHeight + "px + "+headerMessageHead+"px + "+15+"px))";
    }
  });


})();
// $(window).on("load", function () {
//   $(".list-tovar, region-select, .scroll-default, .scr-el").mCustomScrollbar({
//     axis: "y",
//     theme: "custom-el",
//     onTotalScrollOffset: 60,
//     callbacks: {
//       onCreate: function () {
//         $(this).find(".item").css("width", $(this).width());
//       },
//       onBeforeUpdate: function () {
//         $(this).find(".item").css("width", $(this).width());
//       }
//     }
//   });
// });

// (function () {
//
//
//
//
//
//
//   if ($('.object__box').length > 3) {
//     $('.object--slider').slick({
//       dots: false,
//       infinite: false,
//       slidesToScroll: 3,
//       slidesToShow: 3,
//       speed: 300,
//       nextArrow: '<button type="button" aria-label="Следующий" class="slick-next"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 1L15 16.4312L2 30" stroke="#AEA69D" stroke-opacity="0.3" stroke-width="3"/></svg></button>',
//       prevArrow: '<button type="button" aria-label="Предыдущий" class="slick-left"><svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 31L3 15.5688L16 2" stroke="#AEA69D" stroke-opacity="0.3" stroke-width="3"/></svg></button>',
//       responsive: [
//         {
//           breakpoint: 1200,
//           settings: {
//             variableWidth: true,
//             arrows: false,
//           }
//         },
//         {
//           breakpoint: 992,
//           settings: {
//             variableWidth: true,
//             slidesToScroll: 2,
//           }
//         },
//       ]
//     });
//   }
//
//   $('.collapse-elements .link-open').click(function() {
//     let dropDown = $(this).closest('.collapse-element').find('.collapse-element-body');
//     $(this).closest('.collapse-elements').find('.collapse-element-body').not(dropDown).slideUp();
//     if ($(this).hasClass("active")) {
//       $(this).removeClass("active");
//     } else {
//       $(this).closest('.collapse-element').find('.link-open.active').removeClass('active');
//       $(this).addClass('active');
//     }
//     dropDown.stop(false, true).slideToggle();
//   });
//
//   $(document).on('click', '.hide-button', function (e) {
//     e.preventDefault();
//     $(this).toggleClass('active-hide');
//     if ($(this).hasClass('active-hide')) {
//       $(this).parents('.feedback__item').find('.feedback__body').slideToggle();
//       $(this).text('Показать отзывы');
//       $(this).parents('.feedback__item').find('.feedback__body').removeClass('hide');
//     } else {
//       $(this).parents('.feedback__item').find('.feedback__body').slideToggle();
//       $(this).parents('.feedback__item').find('.feedback__body').addClass('hide');
//       $(this).text('Скрыть отзывы');
//     }
//   });
//
//   $('[data-toggle="tooltip"]').tooltip();
//   $('.dragAndrop').sortable({
//     items: '> .category-edit',
//     forcePlaceholderSize: true,
//     handle: '.handle',
//     placeholder: 'sortable-placeholder',
//     start: function (e, ui) {
//       ui.placeholder.height(ui.item.height());
//     },
//   });
//   $(".dragAndrop").disableSelection();
// });
//
// // (function () {
// //
// //   //  модалки
// //   $(document).on('show.bs.modal', '.modal', function () {
// //     let zIndex = 1040 + (10 * $('.modal:visible').length);
// //     $(this).css('z-index', zIndex);
// //     setTimeout(function () {
// //       $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
// //     }, 0);
// //   });
// //
// //   //фильтр btn
// // 	let filterBtn = document.querySelector('.controls');
// // 	if (filterBtn) {
// // 		filterBtn.onclick = function(e) {
// // 			let filterBtn = e.target.closest('.filter-button');
// //
// // 			if (!filterBtn) return;
// // 			filterBtn.nextElementSibling.nextElementSibling.classList.toggle('active')
// // 		}
// // 	}
// //
// //   $(document).on('hidden.bs.modal', '.modal', function () {
// //     $('.modal:visible').length && $(document.body).addClass('modal-open');
// //   });
// //
// //   $('.gnr-tree__name').click(function() {
// // 		if ($(this).hasClass('active')) {
// // 			$(this).removeClass('active');
// // 			$(this).parent().children('.gnr-tree__group').slideUp(200);
// // 			// $(this).children().slideUp();
// // 		} else {
// // 			$(this).addClass('active');
// // 			$(this).parent().children('.gnr-tree__group').slideDown(200);
// // 		}
// // 	});
// //
// //   $('.flex-menu').flexMenu({
// // 		cutoff: 2,
// // 		showOnHover: false,
// // 		// showOnHover: false,
// // 		linkText: "<i class='my-icon'></i>",
// // 		popupClass: 'more_dropdown',
// // 		linkTextAll: "Меню",
// // 	});
// //
// //   // слайдер для главной страницы
// // 	if ($(window).width() < 768) {
// // 		$('.section-step .row').slick({
// // 			slidesToScroll: 1,
// // 			slidesToShow: 1,
// // 			dots: true,
// // 			infinite: true,
// // 			speed: 300,
// // 			arrows: false,
// // 			adaptiveHeight: true,
// // 			fade: true,
// // 			responsive: [
// // 				{
// // 					breakpoint: 480,
// // 					settings: {
// // 						fade: false
// // 					}
// // 				}
// // 			]
// // 		});
// // 	}
// // });
// //
// //

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkNhbGVuZGFyLmpzIiwiYXBwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1ZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKCcjdGVzdDEnKS5kYXRlcGlja2VyKHtcclxuICAvLyDQnNC+0LbQvdC+INCy0YvQsdGA0LDRgtGMINGC0L7Qu9GM0L4g0LTQsNGC0YssINC40LTRg9GJ0LjQtSDQt9CwINGB0LXQs9C+0LTQvdGP0YjQvdC40Lwg0LTQvdC10LwsINCy0LrQu9GO0YfQsNGPINGB0LXQs9C+0LTQvdGPXHJcbiAgbWluRGF0ZTogbmV3IERhdGUoKSxcclxuICBhdXRvQ2xvc2U6IGZhbHNlLFxyXG59KTtcclxuXHJcblxyXG4kKCcjdGVzdC10aW1lcGlja2VyJykudGltZXBpY2tlcih7XHJcbiAgdGltZUZvcm1hdDogJ0g6bW0nLFxyXG4gIGludGVydmFsOiAzMCxcclxuICBkZWZhdWx0VGltZTogJzgnLFxyXG4gIGR5bmFtaWM6IGZhbHNlLFxyXG4gIHNjcm9sbGJhcjogdHJ1ZSxcclxufSk7XHJcblxyXG4vLyAvL1xyXG4vLyAvLyAkKCcubW9kYWwnKS5vbignc2Nyb2xsJywgZnVuY3Rpb24oZSkge1xyXG4vLyAvLyAgIGlmICgkKCcuZGF0ZXBpY2tlcicpLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xyXG4vLyAvLyAgICAgJCgnLmRhdGVwaWNrZXInKS5yZW1vdmVDbGFzcygnYWN0aXZlJylcclxuLy8gLy8gICB9XHJcbi8vIC8vIH0pXHJcbi8vIC8vXHJcbi8vIC8vIC8vIGZ1bmN0aW9uIGNyZWF0ZUVsZW1lbnQodGFnLCBwcm9wcywgLi4uY2hpbGRyZW4pIHtcclxuLy8gLy8gLy8gICBjb25zdCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCh0YWcpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgIGlmIChwcm9wcykge1xyXG4vLyAvLyAvLyAgICAgT2JqZWN0LmVudHJpZXMocHJvcHMpLmZvckVhY2goKFtrZXksIHZhbHVlXSkgPT4ge1xyXG4vLyAvLyAvLyAgICAgICBpZiAoa2V5LnN0YXJ0c1dpdGgoJ29uJykgJiYgdHlwZW9mIHZhbHVlID09PSAnZnVuY3Rpb24nKSB7XHJcbi8vIC8vIC8vICAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKGtleS5zdWJzdHJpbmcoMiksIHZhbHVlKTtcclxuLy8gLy8gLy8gICAgICAgfSBlbHNlIGlmIChrZXkuc3RhcnRzV2l0aCgnZGF0YS0nKSkge1xyXG4vLyAvLyAvLyAgICAgICAgIGVsZW1lbnQuc2V0QXR0cmlidXRlKGtleSwgdmFsdWUpO1xyXG4vLyAvLyAvLyAgICAgICB9IGVsc2Uge1xyXG4vLyAvLyAvLyAgICAgICAgIGVsZW1lbnRba2V5XSA9IHZhbHVlO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vICAgICB9KTtcclxuLy8gLy8gLy8gICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgY2hpbGRyZW4uZm9yRWFjaChjaGlsZCA9PiB7XHJcbi8vIC8vIC8vICAgICBpZiAoQXJyYXkuaXNBcnJheShjaGlsZCkpIHtcclxuLy8gLy8gLy8gICAgICAgcmV0dXJuIGVsZW1lbnQuYXBwZW5kKC4uLmNoaWxkKTtcclxuLy8gLy8gLy8gICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgIGlmICh0eXBlb2YgY2hpbGQgPT09ICdzdHJpbmcnIHx8IHR5cGVvZiBjaGlsZCA9PT0gJ251bWJlcicpIHtcclxuLy8gLy8gLy8gICAgICAgY2hpbGQgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjaGlsZCk7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBOb2RlKSB7XHJcbi8vIC8vIC8vICAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQoY2hpbGQpO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvLyAgIH0pO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgIHJldHVybiBlbGVtZW50O1xyXG4vLyAvLyAvLyB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vIGxldCBkYXRlT25lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3Rlc3QxJyk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vIGlmIChkYXRlT25lKSB7XHJcbi8vIC8vIC8vICAgY2xhc3MgQ2FsZW5kYXIge1xyXG4vLyAvLyAvLyAgICAgY29uc3RydWN0b3IoZWxlbWVudCwgb2JqKSB7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMucGFyZW50RWwgPSAkKCdib2R5Jyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMuZWxlbWVudCA9ICQoZWxlbWVudCk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMuc3RhcnREYXRlID0gbW9tZW50KCkuc3RhcnRPZignZGF5Jyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMuZW5kRGF0ZSA9IG1vbWVudCgpLmVuZE9mKCdkYXknKTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdGhpcy5taW5EYXRlID0gZmFsc2U7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMubWF4U3BhbiA9IGZhbHNlO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLm1pblllYXIgPSBtb21lbnQoKS5jbG9uZSgpLnN1YnRyYWN0KDEsICd5ZWFyJykuZm9ybWF0KCdZWVlZJyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMubWF4WWVhciA9IG1vbWVudCgpLmNsb25lKCkuYWRkKDEsICd5ZWFyJykuZm9ybWF0KCdZWVlZJyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMubGVmdENhbGVuZGFyID0ge307XHJcbi8vIC8vIC8vICAgICAgIHRoaXMucmlnaHRDYWxlbmRhciA9IHt9O1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmlzU2hvd2luZyA9IGZhbHNlO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAvLyDQvtC00LjQvSDQutCw0LvQtdC90LTQsNGA0YxcclxuLy8gLy8gLy8gICAgICAgdGhpcy5zaW5nbGVDYWxlbmRhciA9IGZhbHNlO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodHlwZW9mIG9iaiAhPT0gJ29iamVjdCcgfHwgb2JqID09PSBudWxsKSB7XHJcbi8vIC8vIC8vICAgICAgICAgb2JqID0ge307XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdGhpcy50ZW1wbGF0ZSA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsIHsgY2xhc3NOYW1lOiAnY2FsZW5kYXInIH0sXHJcbi8vIC8vIC8vICAgICAgICAgY3JlYXRlRWxlbWVudCgnZGl2JywgeyBjbGFzc05hbWU6IFwiY2FsZW5kYXItaW5uZXJcIiB9LFxyXG4vLyAvLyAvLyAgICAgICAgICAgY3JlYXRlRWxlbWVudCgnZGl2JywgeyBjbGFzc05hbWU6ICdjYWxlbmRhci1kcnAgbGVmdCcgfSxcclxuLy8gLy8gLy8gICAgICAgICAgICAgY3JlYXRlRWxlbWVudCgnZGl2JywgeyBjbGFzc05hbWU6ICdjYWxlbmRhci1oZWFkJyB9LCksXHJcbi8vIC8vIC8vICAgICAgICAgICAgIGNyZWF0ZUVsZW1lbnQoJ2RpdicsIHtjbGFzc05hbWU6ICdjYWxlbmRhci10YWJsZSd9KSxcclxuLy8gLy8gLy8gICAgICAgICAgICksXHJcbi8vIC8vIC8vICAgICAgICAgICBjcmVhdGVFbGVtZW50KCdkaXYnLCB7Y2xhc3NOYW1lOiAnY2FsZW5kYXItZHJwIHJpZ2h0J30sXHJcbi8vIC8vIC8vICAgICAgICAgICAgIGNyZWF0ZUVsZW1lbnQoJ2RpdicsIHtjbGFzc05hbWU6ICdjYWxlbmRhci1oZWFkJ30sXHJcbi8vIC8vIC8vICAgICAgICAgICAgICAgY3JlYXRlRWxlbWVudCgnZGl2JywgeyBjbGFzc05hbWU6ICdjYWxlbmRhci1oZWFkLXdyYXAgY2FsZW5kYXItaGVhZC13cmFwLW9wdGlvbid9KVxyXG4vLyAvLyAvLyAgICAgICAgICAgICApLFxyXG4vLyAvLyAvLyAgICAgICAgICAgICBjcmVhdGVFbGVtZW50KCdkaXYnLCB7XHJcbi8vIC8vIC8vICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnY2FsZW5kYXItdGFibGUnXHJcbi8vIC8vIC8vICAgICAgICAgICAgIH0pLFxyXG4vLyAvLyAvLyAgICAgICAgICAgKSxcclxuLy8gLy8gLy8gICAgICAgICApXHJcbi8vIC8vIC8vICAgICAgICk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMuY2xpY2tEYXRlID0gdGhpcy5jbGlja0RhdGUuYmluZCh0aGlzKTtcclxuLy8gLy8gLy8gICAgICAgdGhpcy5zaG93ID0gdGhpcy5zaG93LmJpbmQodGhpcyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMudG9nZ2xlID0gdGhpcy50b2dnbGUuYmluZCh0aGlzKTtcclxuLy8gLy8gLy8gICAgICAgdGhpcy5rZXlkb3duID0gdGhpcy5rZXlkb3duLmJpbmQodGhpcyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMub3V0c2lkZUNsaWNrID0gdGhpcy5vdXRzaWRlQ2xpY2suYmluZCh0aGlzKTtcclxuLy8gLy8gLy8gICAgICAgdGhpcy5jbGlja1ByZXYgPSB0aGlzLmNsaWNrUHJldi5iaW5kKHRoaXMpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmNsaWNrTmV4dCA9IHRoaXMuY2xpY2tOZXh0LmJpbmQodGhpcyk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMubG9jYWxlID0ge1xyXG4vLyAvLyAvLyAgICAgICAgIGRheXNPZldlZWs6ICBtb21lbnQubG9jYWxlRGF0YSgncnUnKS53ZWVrZGF5c01pbigpLFxyXG4vLyAvLyAvLyAgICAgICAgIG1vbnRoTmFtZXM6IG1vbWVudC5sb2NhbGVEYXRhKCdydScpLm1vbnRocygpLFxyXG4vLyAvLyAvLyAgICAgICB9O1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBsZXQgaW5zZXJ0ID0gdGhpcy5lbGVtZW50LnBhcmVudCgpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmNvbnRhaW5lciA9ICQodGhpcy50ZW1wbGF0ZSkuYXBwZW5kVG8oaW5zZXJ0KTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdGhpcy5jb250YWluZXIuZmluZCgnLmNhbGVuZGFyLWRycCcpXHJcbi8vIC8vIC8vICAgICAgICAgLm9uKCdtb3VzZWRvd24uY2FsZW5kYXInLCAndGQuYXZhaWxhYmxlJywgdGhpcy5jbGlja0RhdGUpXHJcbi8vIC8vIC8vICAgICAgICAgLm9uKCdjbGljay5jYWxlbmRhcicsICcucHJldicsIHRoaXMuY2xpY2tQcmV2KVxyXG4vLyAvLyAvLyAgICAgICAgIC5vbignY2xpY2suY2FsZW5kYXInLCAnLm5leHQgJywgdGhpcy5jbGlja05leHQpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodHlwZW9mIG9iai5zaW5nbGVDYWxlbmRhciA9PT0gJ2Jvb2xlYW4nKSB7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5zaW5nbGVDYWxlbmRhciA9IG9iai5zaW5nbGVDYWxlbmRhcjtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICBpZiAodGhpcy5zaW5nbGVDYWxlbmRhcikge1xyXG4vLyAvLyAvLyAgICAgICAgICAgdGhpcy5lbmREYXRlID0gdGhpcy5zdGFydERhdGUuY2xvbmUoKVxyXG4vLyAvLyAvLyAgICAgICAgIH1cclxuLy8gLy8gLy8gICAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodGhpcy5zaW5nbGVDYWxlbmRhcikge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuY29udGFpbmVyLmZpbmQoJy5jYWxlbmRhci1kcnAucmlnaHQnKS5oaWRlKCk7XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgLy8g0LfQvdCw0YfQtdC90LjQtSDQuNC3IGlucHV0XHJcbi8vIC8vIC8vICAgICAgIGxldCBzdGFydCwgZW5kO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAoJCh0aGlzLmVsZW1lbnQpLmlzKCc6dGV4dCcpKSB7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgICAgbGV0IHZhbCA9ICQodGhpcy5lbGVtZW50KS52YWwoKSxcclxuLy8gLy8gLy8gICAgICAgICAgIHNwbGl0ID0gdmFsLnNwbGl0KCcgLSAnKTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICBzdGFydCA9IGVuZCA9IG51bGw7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgICAgaWYgKHNwbGl0Lmxlbmd0aCA9PSAyKSB7XHJcbi8vIC8vIC8vICAgICAgICAgICBzdGFydCA9IG1vbWVudChzcGxpdFswXSwgJ0RELk1NLllZWVknKS5jbG9uZSgpO1xyXG4vLyAvLyAvLyAgICAgICAgICAgZW5kID0gXHRtb21lbnQoc3BsaXRbMV0sICdERC5NTS5ZWVlZJykuY2xvbmUoKVxyXG4vLyAvLyAvLyAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zaW5nbGVDYWxlbmRhciAmJiB2YWwgIT09ICcnKSB7XHJcbi8vIC8vIC8vICAgICAgICAgICBzdGFydCA9IG1vbWVudCh2YWwsICdERC5NTS5ZWVlZJyk7XHJcbi8vIC8vIC8vICAgICAgICAgICBlbmQgPSBtb21lbnQodmFsLCAnREQuTU0uWVlZWScpO1xyXG4vLyAvLyAvLyAgICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICBpZiAoc3RhcnQgIT09IG51bGwgJiYgZW5kICE9PSBudWxsKSB7XHJcbi8vIC8vIC8vICAgICAgICAgICB0aGlzLnNldFN0YXJ0RGF0ZShzdGFydCk7XHJcbi8vIC8vIC8vICAgICAgICAgICB0aGlzLnNldEVuZERhdGUoZW5kKTtcclxuLy8gLy8gLy8gICAgICAgICB9XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgaWYgKHRoaXMuZWxlbWVudC5pcyhcImlucHV0XCIpKSB7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5lbGVtZW50Lm9uKHtcclxuLy8gLy8gLy8gICAgICAgICAgICdjbGljay5jYWxlbmRhcic6IHRoaXMuc2hvdyxcclxuLy8gLy8gLy8gICAgICAgICAgICdmb2N1cy5jYWxlbmRhcic6IHRoaXMuc2hvdyxcclxuLy8gLy8gLy8gICAgICAgICAgICdrZXlkb3duLmNhbGVuZGFyJzogdGhpcy5rZXlkb3duXHJcbi8vIC8vIC8vICAgICAgICAgfSlcclxuLy8gLy8gLy8gICAgICAgfSBlbHNlIHtcclxuLy8gLy8gLy8gICAgICAgICB0aGlzLmVsZW1lbnQub24oJ2NsaWNrLmNhbGVuZGFyJywgdGhpcy50b2dnbGUpXHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5lbGVtZW50Lm9uKCdrZXlkb3duLmNhbGVuZGFyJywgdGhpcy50b2dnbGUpXHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdGhpcy51cGRhdGVFbGVtZW50KCk7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBzZXRTdGFydERhdGUoc3RhcnREYXRlKSB7XHJcbi8vIC8vIC8vICAgICAgIGlmICh0eXBlb2Ygc3RhcnREYXRlID09PSAnc3RyaW5nJykge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuc3RhcnREYXRlID0gbW9tZW50KHN0YXJ0RGF0ZSwgbW9tZW50LmZvcm1hdCgnREQuTU0uWVlZWSBISDptbScpKTtcclxuLy8gLy8gLy8gICAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodHlwZW9mIHN0YXJ0RGF0ZSA9PT0gJ29iamVjdCcpIHtcclxuLy8gLy8gLy8gICAgICAgICB0aGlzLnN0YXJ0RGF0ZSA9IG1vbWVudChzdGFydERhdGUpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMudXBkYXRlTW9udGhzSW5WaWV3KCk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBzZXRFbmREYXRlIChlbmREYXRlKSB7XHJcbi8vIC8vIC8vICAgICAgIGlmICh0eXBlb2YgZW5kRGF0ZSA9PT0gJ3N0cmluZycpIHtcclxuLy8gLy8gLy8gICAgICAgICB0aGlzLmVuZERhdGUgPSBtb21lbnQoc3RhcnREYXRlLCBtb21lbnQuZm9ybWF0KCdERC5NTS5ZWVlZIEhIOm1tJykpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIGlmICh0eXBlb2YgZW5kRGF0ZSA9PT0gJ29iamVjdCcpIHtcclxuLy8gLy8gLy8gICAgICAgICB0aGlzLmVuZERhdGUgPSBtb21lbnQoZW5kRGF0ZSk7XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgaWYgKHRoaXMuZW5kRGF0ZS5pc0JlZm9yZSh0aGlzLnN0YXJ0RGF0ZSkpIHtcclxuLy8gLy8gLy8gICAgICAgICB0aGlzLmVuZERhdGUgPSB0aGlzLnN0YXJ0RGF0ZS5jbG9uZSgpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIGlmICh0aGlzLm1heFNwYW4gJiYgdGhpcy5zdGFydERhdGUuY2xvbmUoKS5hZGQodGhpcy5tYXhTcGFuKS5pc0JlZm9yZSh0aGlzLmVuZERhdGUpKSB7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5lbmREYXRlID0gdGhpcy5zdGFydERhdGUuY2xvbmUoKS5hZGQodGhpcy5tYXhTcGFuKTtcclxuLy8gLy8gLy8gICAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLnVwZGF0ZU1vbnRoc0luVmlldygpO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgaXNJbnZhbGlkRGF0ZSgpIHtcclxuLy8gLy8gLy8gICAgICAgcmV0dXJuIGZhbHNlO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgaXNDdXN0b21EYXRlKCkge1xyXG4vLyAvLyAvLyAgICAgICByZXR1cm4gZmFsc2U7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICB1cGRhdGVWaWV3KCkge1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLnVwZGF0ZU1vbnRoc0luVmlldygpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLnVwZGF0ZUNhbGVuZGFycygpO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgdXBkYXRlTW9udGhzSW5WaWV3KCkge1xyXG4vLyAvLyAvLyAgICAgICBpZiAodGhpcy5lbmREYXRlKSB7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5sZWZ0Q2FsZW5kYXIubW9udGggPSB0aGlzLnN0YXJ0RGF0ZS5jbG9uZSgpLmRhdGUoMik7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgICAgaWYgKHRoaXMuZW5kRGF0ZS5tb250aCgpICE9IHRoaXMuc3RhcnREYXRlLm1vbnRoKCkgfHwgdGhpcy5lbmREYXRlLnllYXIoKSAhPSB0aGlzLnN0YXJ0RGF0ZS55ZWFyKCkpIHtcclxuLy8gLy8gLy8gICAgICAgICAgIHRoaXMucmlnaHRDYWxlbmRhci5tb250aCA9IHRoaXMuZW5kRGF0ZS5jbG9uZSgpLmRhdGUoMik7XHJcbi8vIC8vIC8vICAgICAgICAgfSBlbHNlIHtcclxuLy8gLy8gLy8gICAgICAgICAgIHRoaXMucmlnaHRDYWxlbmRhci5tb250aCA9IHRoaXMuc3RhcnREYXRlLmNsb25lKCkuZGF0ZSgyKS5hZGQoMSwgJ21vbnRoJyk7XHJcbi8vIC8vIC8vICAgICAgICAgfVxyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICB1cGRhdGVDYWxlbmRhcnMoKSB7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMucmVuZGVyQ2FsZW5kYXIoJ2xlZnQnKTtcclxuLy8gLy8gLy8gICAgICAgdGhpcy5yZW5kZXJDYWxlbmRhcigncmlnaHQnKTtcclxuLy8gLy8gLy8gICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgIHJlbmRlckNhbGVuZGFyKHNpZGUpIHtcclxuLy8gLy8gLy8gICAgICAgdmFyIGNhbGVuZGFyID0gc2lkZSA9PSAnbGVmdCcgPyB0aGlzLmxlZnRDYWxlbmRhciA6IHRoaXMucmlnaHRDYWxlbmRhcjtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdmFyIG1vbnRoID0gY2FsZW5kYXIubW9udGgubW9udGgoKTtcclxuLy8gLy8gLy8gICAgICAgdmFyIHllYXIgPSBjYWxlbmRhci5tb250aC55ZWFyKCk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHZhciBkYXlzSW5Nb250aCA9IG1vbWVudChbeWVhciwgbW9udGhdKS5kYXlzSW5Nb250aCgpO1xyXG4vLyAvLyAvLyAgICAgICB2YXIgZmlyc3REYXkgPSBtb21lbnQoW3llYXIsIG1vbnRoLCAxXSk7XHJcbi8vIC8vIC8vICAgICAgIHZhciBsYXN0RGF5ID0gbW9tZW50KFt5ZWFyLCBtb250aCwgZGF5c0luTW9udGhdKTtcclxuLy8gLy8gLy8gICAgICAgdmFyIGxhc3RNb250aCA9IG1vbWVudChmaXJzdERheSkuc3VidHJhY3QoMSwgJ21vbnRoJykubW9udGgoKTtcclxuLy8gLy8gLy8gICAgICAgdmFyIGxhc3RZZWFyID0gbW9tZW50KGZpcnN0RGF5KS5zdWJ0cmFjdCgxLCAnbW9udGgnKS55ZWFyKCk7XHJcbi8vIC8vIC8vICAgICAgIHZhciBkYXlzSW5MYXN0TW9udGggPSBtb21lbnQoW2xhc3RZZWFyLCBsYXN0TW9udGhdKS5kYXlzSW5Nb250aCgpO1xyXG4vLyAvLyAvLyAgICAgICB2YXIgZGF5T2ZXZWVrID0gZmlyc3REYXkuZGF5KCk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHZhciBob3VyID0gY2FsZW5kYXIubW9udGguaG91cigpO1xyXG4vLyAvLyAvLyAgICAgICB2YXIgbWludXRlID0gY2FsZW5kYXIubW9udGgubWludXRlKCk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHZhciBjYWxlbmRhciA9IFtdO1xyXG4vLyAvLyAvLyAgICAgICBjYWxlbmRhci5maXJzdERheSA9IGZpcnN0RGF5O1xyXG4vLyAvLyAvLyAgICAgICBjYWxlbmRhci5sYXN0RGF5ID0gbGFzdERheTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCA2OyBpKyspIHtcclxuLy8gLy8gLy8gICAgICAgICBjYWxlbmRhcltpXSA9IFtdO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHZhciBzdGFydERheSA9IGRheXNJbkxhc3RNb250aCAtIGRheU9mV2VlayArIG1vbWVudC5sb2NhbGVEYXRhKCdydScpLmZpcnN0RGF5T2ZXZWVrKCkgKyAxO1xyXG4vLyAvLyAvLyAgICAgICBpZiAoc3RhcnREYXkgPiBkYXlzSW5MYXN0TW9udGgpXHJcbi8vIC8vIC8vICAgICAgICAgc3RhcnREYXkgLT0gNztcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgaWYgKGRheU9mV2VlayA9PSBtb21lbnQubG9jYWxlRGF0YSgncnUnKS5maXJzdERheU9mV2VlaygpKVxyXG4vLyAvLyAvLyAgICAgICAgIHN0YXJ0RGF5ID0gZGF5c0luTGFzdE1vbnRoIC0gNjtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdmFyIGN1ckRhdGUgPSBtb21lbnQoW2xhc3RZZWFyLCBsYXN0TW9udGgsIHN0YXJ0RGF5LCAxMiwgbWludXRlXSk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHZhciBjb2wsIHJvdztcclxuLy8gLy8gLy8gICAgICAgZm9yICh2YXIgaSA9IDAsIGNvbCA9IDAsIHJvdyA9IDA7IGkgPCA0MjsgaSsrLCBjb2wrKywgY3VyRGF0ZSA9IG1vbWVudChjdXJEYXRlKS5hZGQoMjQsICdob3VyJykpIHtcclxuLy8gLy8gLy8gICAgICAgICBpZiAoaSA+IDAgJiYgY29sICUgNyA9PT0gMCkge1xyXG4vLyAvLyAvLyAgICAgICAgICAgY29sID0gMDtcclxuLy8gLy8gLy8gICAgICAgICAgIHJvdysrO1xyXG4vLyAvLyAvLyAgICAgICAgIH1cclxuLy8gLy8gLy8gICAgICAgICBjYWxlbmRhcltyb3ddW2NvbF0gPSBjdXJEYXRlLmNsb25lKCkuaG91cihob3VyKS5taW51dGUobWludXRlKTtcclxuLy8gLy8gLy8gICAgICAgICBjdXJEYXRlLmhvdXIoMTIpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIC8vIG1ha2UgdGhlIGNhbGVuZGFyIG9iamVjdCBhdmFpbGFibGUgdG8gaG92ZXJEYXRlL2NsaWNrRGF0ZVxyXG4vLyAvLyAvLyAgICAgICBpZiAoc2lkZSA9PSAnbGVmdCcpIHtcclxuLy8gLy8gLy8gICAgICAgICB0aGlzLmxlZnRDYWxlbmRhci5jYWxlbmRhciA9IGNhbGVuZGFyO1xyXG4vLyAvLyAvLyAgICAgICB9IGVsc2Uge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMucmlnaHRDYWxlbmRhci5jYWxlbmRhciA9IGNhbGVuZGFyO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIC8v0LLRi9Cx0L7RgCDQstGA0LXQvNC10L3QuCDQuCDQtNCw0YLRi1xyXG4vLyAvLyAvLyAgICAgICB2YXIgY3VycmVudE1vbnRoID0gY2FsZW5kYXJbMV1bMV0ubW9udGgoKTtcclxuLy8gLy8gLy8gICAgICAgY29uc29sZS5sb2coY3VycmVudE1vbnRoKVxyXG4vLyAvLyAvLyAgICAgICB2YXIgY3VycmVudFllYXIgPSBjYWxlbmRhclsxXVsxXS55ZWFyKCk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIGxldCBtaW5EYXRlID0gc2lkZSA9PSAnbGVmdCcgP2ZhbHNlOnRoaXMuc3RhcnREYXRlO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICB2YXIgbWF4WWVhciA9IHRoaXMubWF4WWVhcjtcclxuLy8gLy8gLy8gICAgICAgdmFyIG1pblllYXIgPSAobWluRGF0ZSAmJiBtaW5EYXRlLnllYXIoKSkgfHwgKHRoaXMubWluWWVhcik7XHJcbi8vIC8vIC8vICAgICAgIHZhciBpbk1pblllYXIgPSBjdXJyZW50WWVhciA9PSBtaW5ZZWFyO1xyXG4vLyAvLyAvLyAgICAgICB2YXIgaW5NYXhZZWFyID0gY3VycmVudFllYXIgPT0gbWF4WWVhcjtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy9cclxuLy8gLy8gLy9cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdmFyIG5hbWVNb250aCA9IG1vbWVudC5sb2NhbGVEYXRhKCdydScpLm1vbnRocygpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICB2YXIgbW9udGhIdG1sID0gXCI8ZGl2IGNsYXNzPSdjYWxlbmRhci1oZWFkLXdyYXAnPlwiO1xyXG4vLyAvLyAvLyAgICAgICBtb250aEh0bWwgKz0gJzxidXR0b24gY2xhc3M9XCJwcmV2IGF2YWlsYWJsZVwiPjxzcGFuPjw8L3NwYW4+PC9idXR0b24+JztcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdmFyIGRhdGVIdG1sID0gdGhpcy5sb2NhbGUubW9udGhOYW1lc1tjYWxlbmRhclsxXVsxXS5tb250aCgpXSArIGNhbGVuZGFyWzFdWzFdLmZvcm1hdChcIiBZWVlZXCIpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBtb250aEh0bWwgKz0gJzxkaXYgY2xhc3M9XCJjYWxlbmRhci1jYXB0aW9uXCI+JyArIGRhdGVIdG1sICsgJzwvZGl2Pic7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIG1vbnRoSHRtbCArPSAnPGJ1dHRvbiBjbGFzcz1cIm5leHQgYXZhaWxhYmxlXCI+PHNwYW4+Pjwvc3Bhbj48L2J1dHRvbj4nO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmNvbnRhaW5lci5maW5kKCcuY2FsZW5kYXItZHJwLicgKyBzaWRlICsgJyAuY2FsZW5kYXItaGVhZCcpLmh0bWwobW9udGhIdG1sKTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdmFyIGh0bWwgPSAnPHRhYmxlPic7XHJcbi8vIC8vIC8vICAgICAgIGh0bWwgKz0gJzx0aGVhZD4nO1xyXG4vLyAvLyAvLyAgICAgICBodG1sICs9ICc8dHI+JztcclxuLy8gLy8gLy8gICAgICAgJC5lYWNoKHRoaXMubG9jYWxlLmRheXNPZldlZWssIGZ1bmN0aW9uKGluZGV4LCBkYXlPZldlZWspIHtcclxuLy8gLy8gLy8gICAgICAgICBodG1sICs9ICc8dGg+JyArIGRheU9mV2VlayArICc8L3RoPic7XHJcbi8vIC8vIC8vICAgICAgIH0pO1xyXG4vLyAvLyAvLyAgICAgICBodG1sICs9ICc8dHI+JztcclxuLy8gLy8gLy8gICAgICAgaHRtbCArPSAnPC90aGVhZD4nO1xyXG4vLyAvLyAvLyAgICAgICBodG1sICs9ICc8dGJvZHk+JztcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgZm9yIChsZXQgcm93ID0gMDsgcm93IDwgNjsgcm93KyspIHtcclxuLy8gLy8gLy8gICAgICAgICBodG1sICs9ICc8dHI+JztcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICAvLyBhZGQgd2VlayBudW1iZXJcclxuLy8gLy8gLy8gICAgICAgICBmb3IgKGxldCBjb2wgPSAwOyBjb2wgPCA3OyBjb2wrKykge1xyXG4vLyAvLyAvLyAgICAgICAgICAgbGV0IGNsYXNzZXMgPSBbXTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICAgIC8vINCw0LrRgtC40LLQvdGL0LlcclxuLy8gLy8gLy8gICAgICAgICAgIGlmIChjYWxlbmRhcltyb3ddW2NvbF0uZm9ybWF0KCdERC5NTS5ZWVlZJykgPT0gdGhpcy5zdGFydERhdGUuZm9ybWF0KCdERC5NTS5ZWVlZJykpIHtcclxuLy8gLy8gLy8gICAgICAgICAgICAgY2xhc3Nlcy5wdXNoKCdhY3RpdmUnLCAnc3RhcnQtZGF0ZScpO1xyXG4vLyAvLyAvLyAgICAgICAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAgICAgLy90b2RheVxyXG4vLyAvLyAvLyAgICAgICAgICAgaWYgKGNhbGVuZGFyW3Jvd11bY29sXS5pc1NhbWUobmV3IERhdGUoKSwgXCJkYXlcIikpXHJcbi8vIC8vIC8vICAgICAgICAgICAgIGNsYXNzZXMucHVzaCgndG9kYXknKTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICAgIC8vaGlnaGxpZ2h0IHRoZSBjdXJyZW50bHkgc2VsZWN0ZWQgZW5kIGRhdGVcclxuLy8gLy8gLy8gICAgICAgICAgIGlmICh0aGlzLmVuZERhdGUgIT0gbnVsbCAmJiBjYWxlbmRhcltyb3ddW2NvbF0uZm9ybWF0KCdERC5NTS5ZWVlZJykgPT0gdGhpcy5lbmREYXRlLmZvcm1hdCgnREQuTU0uWVlZWScpKVxyXG4vLyAvLyAvLyAgICAgICAgICAgICBjbGFzc2VzLnB1c2goJ2FjdGl2ZScsICdlbmQtZGF0ZScpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAgICAgLy8gd2Vla2VuZHNcclxuLy8gLy8gLy8gICAgICAgICAgIGlmIChjYWxlbmRhcltyb3ddW2NvbF0uaXNvV2Vla2RheSgpID4gNSlcclxuLy8gLy8gLy8gICAgICAgICAgICAgY2xhc3Nlcy5wdXNoKCd3ZWVrZW5kJyk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgICAgICAvL2hpZ2hsaWdodCBkYXRlcyBpbi1iZXR3ZWVuIHRoZSBzZWxlY3RlZCBkYXRlc1xyXG4vLyAvLyAvLyAgICAgICAgICAgaWYgKHRoaXMuZW5kRGF0ZSAhPSBudWxsICYmIGNhbGVuZGFyW3Jvd11bY29sXSA+IHRoaXMuc3RhcnREYXRlICYmIGNhbGVuZGFyW3Jvd11bY29sXSA8IHRoaXMuZW5kRGF0ZSlcclxuLy8gLy8gLy8gICAgICAgICAgICAgY2xhc3Nlcy5wdXNoKCdpbi1yYW5nZScpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAgICAgLy9ncmV5IG91dCB0aGUgZGF0ZXMgaW4gb3RoZXIgbW9udGhzIGRpc3BsYXllZCBhdCBiZWdpbm5pbmcgYW5kIGVuZCBvZiB0aGlzIGNhbGVuZGFyXHJcbi8vIC8vIC8vICAgICAgICAgICBpZiAoY2FsZW5kYXJbcm93XVtjb2xdLm1vbnRoKCkgIT0gY2FsZW5kYXJbMV1bMV0ubW9udGgoKSlcclxuLy8gLy8gLy8gICAgICAgICAgICAgY2xhc3Nlcy5wdXNoKCdvZmYnLCAnZW5kcycpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAgICAgLy9kb24ndCBhbGxvdyBzZWxlY3Rpb24gb2YgZGF0ZSBpZiBhIGN1c3RvbSBmdW5jdGlvbiBkZWNpZGVzIGl0J3MgaW52YWxpZFxyXG4vLyAvLyAvLyAgICAgICAgICAgaWYgKHRoaXMuaXNJbnZhbGlkRGF0ZShjYWxlbmRhcltyb3ddW2NvbF0pKVxyXG4vLyAvLyAvLyAgICAgICAgICAgICBjbGFzc2VzLnB1c2goJ29mZicsICdkaXNhYmxlZCcpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAgICAgLy9hcHBseSBjdXN0b20gY2xhc3NlcyBmb3IgdGhpcyBkYXRlXHJcbi8vIC8vIC8vICAgICAgICAgICB2YXIgaXNDdXN0b20gPSB0aGlzLmlzQ3VzdG9tRGF0ZShjYWxlbmRhcltyb3ddW2NvbF0pO1xyXG4vLyAvLyAvLyAgICAgICAgICAgaWYgKGlzQ3VzdG9tICE9PSBmYWxzZSkge1xyXG4vLyAvLyAvLyAgICAgICAgICAgICBpZiAodHlwZW9mIGlzQ3VzdG9tID09PSAnc3RyaW5nJylcclxuLy8gLy8gLy8gICAgICAgICAgICAgICBjbGFzc2VzLnB1c2goaXNDdXN0b20pO1xyXG4vLyAvLyAvLyAgICAgICAgICAgICBlbHNlXHJcbi8vIC8vIC8vICAgICAgICAgICAgICAgQXJyYXkucHJvdG90eXBlLnB1c2guYXBwbHkoY2xhc3NlcywgaXNDdXN0b20pO1xyXG4vLyAvLyAvLyAgICAgICAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAgICAgdmFyIGNuYW1lID0gJycsIGRpc2FibGVkID0gZmFsc2U7XHJcbi8vIC8vIC8vICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNsYXNzZXMubGVuZ3RoOyBpKyspIHtcclxuLy8gLy8gLy8gICAgICAgICAgICAgY25hbWUgKz0gY2xhc3Nlc1tpXSArICcgJztcclxuLy8gLy8gLy8gICAgICAgICAgICAgaWYgKGNsYXNzZXNbaV0gPT0gJ2Rpc2FibGVkJylcclxuLy8gLy8gLy8gICAgICAgICAgICAgICBkaXNhYmxlZCA9IHRydWU7XHJcbi8vIC8vIC8vICAgICAgICAgICB9XHJcbi8vIC8vIC8vICAgICAgICAgICBpZiAoIWRpc2FibGVkKVxyXG4vLyAvLyAvLyAgICAgICAgICAgICBjbmFtZSArPSAnYXZhaWxhYmxlJztcclxuLy8gLy8gLy8gICAgICAgICAgIGh0bWwgKz0gJzx0ZCBjbGFzcz1cIicgKyBjbmFtZS5yZXBsYWNlKC9eXFxzK3xcXHMrJC9nLCAnJykgKyAnXCIgZGF0YS10aXRsZT1cIicgKyAncicgKyByb3cgKyAnYycgKyBjb2wgKyAnXCI+JyArIGNhbGVuZGFyW3Jvd11bY29sXS5kYXRlKCkgKyAnPC90ZD4nO1xyXG4vLyAvLyAvLyAgICAgICAgIH1cclxuLy8gLy8gLy8gICAgICAgICBodG1sICs9ICc8L3RyPic7XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy8gICAgICAgaHRtbCArPSAnPC90Ym9keT4nO1xyXG4vLyAvLyAvLyAgICAgICBodG1sICs9ICc8L3RhYmxlPic7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMuY29udGFpbmVyLmZpbmQoJy5jYWxlbmRhci1kcnAuJyArIHNpZGUgKyAnIC5jYWxlbmRhci10YWJsZScpLmh0bWwoaHRtbCk7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBtb3ZlKCkge1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmNvbnRhaW5lci5jc3Moe1xyXG4vLyAvLyAvLyAgICAgICAgIHRvcDogODAsXHJcbi8vIC8vIC8vICAgICAgICAgbGVmdDogMCxcclxuLy8gLy8gLy8gICAgICAgICByaWdodDogJ2F1dG8nLFxyXG4vLyAvLyAvLyAgICAgICAgIHZpc2liaWxpdHk6ICd2aXNpYmxlJyxcclxuLy8gLy8gLy8gICAgICAgICBvcGFjaXR5OiAxXHJcbi8vIC8vIC8vICAgICAgIH0pO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgc2hvdygpIHtcclxuLy8gLy8gLy8gICAgICAgaWYgKHRoaXMuaXNTaG93aW5nKSByZXR1cm47XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgICQoZG9jdW1lbnQpXHJcbi8vIC8vIC8vICAgICAgICAgLm9uKCdtb3VzZWRvd24nLCB0aGlzLm91dHNpZGVDbGljaylcclxuLy8gLy8gLy8gICAgICAgICAvL21vYlxyXG4vLyAvLyAvLyAgICAgICAgIC5vbigndG91Y2hlbmQnLCB0aGlzLm91dHNpZGVDbGljaylcclxuLy8gLy8gLy8gICAgICAgICAub24oJ2ZvY3VzaW4nLCB0aGlzLm91dHNpZGVDbGljayk7XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMudXBkYXRlVmlldygpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmNvbnRhaW5lci5zaG93KCk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMubW92ZSgpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmVsZW1lbnQudHJpZ2dlcignc2hvdycsIHRoaXMpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmlzU2hvd2luZyA9IHRydWU7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBoaWRlKCkge1xyXG4vLyAvLyAvLyAgICAgICBpZiAoIXRoaXMuaXNTaG93aW5nKSByZXR1cm47XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIGlmICghdGhpcy5lbmREYXRlKSB7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5zdGFydERhdGUgPSB0aGlzLm9sZFN0YXJ0RGF0ZS5jbG9uZSgpO1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuZW5kRGF0ZSA9IHRoaXMub2xkRW5kRGF0ZS5jbG9uZSgpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIHRoaXMudXBkYXRlRWxlbWVudCgpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICAkKGRvY3VtZW50KS5vZmYoJy5jYWxlbmRhcicpO1xyXG4vLyAvLyAvLyAgICAgICAkKHdpbmRvdykub2ZmKCcuY2FsZW5kYXInKTtcclxuLy8gLy8gLy8gICAgICAgdGhpcy5jb250YWluZXIuaGlkZSgpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmVsZW1lbnQudHJpZ2dlcignaGlkZS5jYWxlbmRhcicsIHRoaXMpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmlzU2hvd2luZyA9IGZhbHNlO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgb3V0c2lkZUNsaWNrKGUpIHtcclxuLy8gLy8gLy8gICAgICAgbGV0IHRhcmdldCA9ICQoZS50YXJnZXQpO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodGFyZ2V0LmNsb3Nlc3QodGhpcy5lbGVtZW50KS5sZW5ndGggfHwgdGFyZ2V0LmNsb3Nlc3QodGhpcy5jb250YWluZXIpLmxlbmd0aCB8fCB0YXJnZXQuY2xvc2VzdCgnLmNhbGVuZGFyLWlubmVyJykubGVuZ3RoKSByZXR1cm5cclxuLy8gLy8gLy8gICAgICAgdGhpcy5oaWRlKCk7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICB0b2dnbGUoKSB7XHJcbi8vIC8vIC8vICAgICAgIGlmICh0aGlzLmlzU2hvd2luZykge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4vLyAvLyAvLyAgICAgICB9IGVsc2Uge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuc2hvdygpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBjbGlja1ByZXYoKSB7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMubGVmdENhbGVuZGFyLm1vbnRoLnN1YnRyYWN0KDEsICdtb250aCcpO1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLnVwZGF0ZUNhbGVuZGFycygpO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgY2xpY2tOZXh0KCkge1xyXG4vLyAvLyAvLyAgICAgICB0aGlzLmxlZnRDYWxlbmRhci5tb250aC5hZGQoMSwgJ21vbnRoJyk7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMudXBkYXRlQ2FsZW5kYXJzKCk7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBjbGlja0RhdGUoZSkge1xyXG4vLyAvLyAvLyAgICAgICBsZXQgdGl0bGUgPSAkKGUudGFyZ2V0KS5hdHRyKCdkYXRhLXRpdGxlJyk7XHJcbi8vIC8vIC8vICAgICAgIGxldCByb3cgPSB0aXRsZS5zdWJzdHIoMSwgMSk7XHJcbi8vIC8vIC8vICAgICAgIGxldCBjb2wgPSB0aXRsZS5zdWJzdHIoMywgMSk7XHJcbi8vIC8vIC8vICAgICAgIGxldCBjYWwgPSAkKGUudGFyZ2V0KS5wYXJlbnRzKCcuY2FsZW5kYXItZHJwJyk7XHJcbi8vIC8vIC8vICAgICAgIGxldCBkYXRlID0gY2FsLmhhc0NsYXNzKCdsZWZ0JykgPyB0aGlzLmxlZnRDYWxlbmRhci5jYWxlbmRhcltyb3ddW2NvbF0gOiB0aGlzLnJpZ2h0Q2FsZW5kYXIuY2FsZW5kYXJbcm93XVtjb2xdO1xyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodGhpcy5lbmREYXRlIHx8IGRhdGUuaXNCZWZvcmUodGhpcy5zdGFydERhdGUsICdkYXknKSkge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuZW5kRGF0ZSA9IG51bGw7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5zZXRTdGFydERhdGUoZGF0ZS5jbG9uZSgpKTtcclxuLy8gLy8gLy8gICAgICAgfVxyXG4vLyAvLyAvL1xyXG4vLyAvLyAvLyAgICAgICBpZiAodGhpcy5zaW5nbGVDYWxlbmRhcikge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuc2V0RW5kRGF0ZSh0aGlzLnN0YXJ0RGF0ZSk7XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgdGhpcy51cGRhdGVWaWV3KCk7XHJcbi8vIC8vIC8vICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICBrZXlkb3duKGUpIHtcclxuLy8gLy8gLy8gICAgICAgLy8gOSAtIHRhYiwgMjcgLSBlc2MsIDI3IC0gZW50ZXJcclxuLy8gLy8gLy8gICAgICAgaWYgKChlLmtleUNvZGUgPT09IDkpIHx8IChlLmtleUNvZGUgPT09IDEzKSkge1xyXG4vLyAvLyAvLyAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICAgIGlmIChlLmtleUNvZGUgPT09IDI3KSB7XHJcbi8vIC8vIC8vICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4vLyAvLyAvLyAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbi8vIC8vIC8vICAgICAgICAgdGhpcy5oaWRlKCk7XHJcbi8vIC8vIC8vICAgICAgIH1cclxuLy8gLy8gLy8gICAgIH1cclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgIHVwZGF0ZUVsZW1lbnQoKSB7XHJcbi8vIC8vIC8vICAgICAgIGlmICh0aGlzLmVsZW1lbnQuaXMoJ2lucHV0JykpIHtcclxuLy8gLy8gLy8gICAgICAgICBsZXQgbmV3VmFsdWUgPSB0aGlzLnN0YXJ0RGF0ZS5mb3JtYXQoJ0RELk1NLllZWVkgSEg6bW0nKTtcclxuLy8gLy8gLy9cclxuLy8gLy8gLy8gICAgICAgICBpZiAobmV3VmFsdWUgIT09IHRoaXMuZWxlbWVudC52YWwoKSkge1xyXG4vLyAvLyAvLyAgICAgICAgICAgdGhpcy5lbGVtZW50LnZhbChuZXdWYWx1ZSkudHJpZ2dlcignY2hhbmdlJyk7XHJcbi8vIC8vIC8vICAgICAgICAgfVxyXG4vLyAvLyAvLyAgICAgICB9XHJcbi8vIC8vIC8vICAgICB9XHJcbi8vIC8vIC8vXHJcbi8vIC8vIC8vICAgICByZW1vdmUoKSB7XHJcbi8vIC8vIC8vICAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZSgpO1xyXG4vLyAvLyAvLyAgICAgfVxyXG4vLyAvLyAvLyAgIH1cclxuLy8gLy8gLy8gICBuZXcgQ2FsZW5kYXIoZGF0ZU9uZSwge1xyXG4vLyAvLyAvLyAgICAgc2luZ2xlQ2FsZW5kYXI6IHRydWVcclxuLy8gLy8gLy8gICB9KTtcclxuLy8gLy8gLy8gfSIsIlxyXG5cclxuXHJcblxyXG4oZnVuY3Rpb24gKCkge1xyXG4gIGxldCBmaWxlSW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuaW5wdXQtZmlsZScpO1xyXG5cclxuICBBcnJheS5mcm9tKGZpbGVJbnB1dCkuZm9yRWFjaChpdGVtID0+IHtcclxuICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24gKGUpIHtcclxuICAgICAgbGV0IF90aGlzID0gdGhpcy5wcmV2aW91c0VsZW1lbnRTaWJsaW5nO1xyXG5cclxuICAgICAgdmFyIHZhbHVlID0gZS50YXJnZXQudmFsdWUubGVuZ3RoID4gMCA/IGUudGFyZ2V0LnZhbHVlIDogX3RoaXMudGV4dENvbnRlbnQ7XHJcbiAgICAgIF90aGlzLnRleHRDb250ZW50ID0gdmFsdWUucmVwbGFjZSgnQzpcXFxcZmFrZXBhdGhcXFxcJywgJycpO1xyXG4gICAgfSlcclxuICB9KTtcclxuXHJcbiAgJCgnLnJlc3VsdC1ibG9jaycpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgY29uc29sZS5sb2coJzIzNCcpO1xyXG4gICAgbGV0IG0gPSBuZXcgRXJyb3IoJ0kgd2FzIGNvbnN0cnVjdGVkIHZpYSB0aGUgXCJuZXdcIiBrZXl3b3JkIScpO1xyXG4gICAgJC5ub3RpZnkobSwge1xyXG4gICAgICBjbGlja1RvSGlkZTogZmFsc2UsXHJcbiAgICAgIGF1dG9IaWRlRGVsYXk6IDEwMDAwMCxcclxuICAgIH0pO1xyXG4gIH0pO1xyXG5cclxuICAvLyDQstGL0YHQvtGC0LBcclxuICBsZXQgYm9keUFkZEhlaWd0aCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jaGF0LWJsb2NrX19ib2R5Jyk7XHJcblxyXG4gIC8vIGxldCBoZWFkZXJNZXNzYWdlSGVhZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jaGF0LWJsb2NrX19yaWdodCcpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodDtcclxuICBsZXQgZm9vdGVyTWVzc2FnZUhlaWdodCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tZXNzYWdlLWlucHV0JykuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0O1xyXG5cclxuICAvLyDRhNC+0YLQutC4INC00LvRjyDRgdC+0L7QsdGJ0LXQvdC40LlcclxuICB2YXIgZmlsZVZpZXcgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnNlbGVjdC1jaG9vc2VcIik7XHJcbiAgdmFyIGZpbGVMaXN0RGlzcGxheSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuY2hhdC12aWV3XCIpO1xyXG4gIHZhciBmaWxlTGlzdCA9IFtdO1xyXG5cclxuICBpZiAoZmlsZVZpZXcpIHtcclxuICAgIGZpbGVWaWV3LmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgZmlsZUxpc3QgPSBbXTtcclxuXHJcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZmlsZVZpZXcuZmlsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBmaWxlTGlzdC5wdXNoKGZpbGVWaWV3LmZpbGVzW2ldKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgZmlsZUxpc3QuZm9yRWFjaChmdW5jdGlvbiAoZmlsZSkge1xyXG4gICAgICAgIHZhciBmaWxlRGlzcGxheUVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcclxuICAgICAgICBmaWxlRGlzcGxheUVsLmNsYXNzTGlzdC5hZGQoJ2ZpbGUtdmlldycpO1xyXG5cclxuICAgICAgICBmaWxlRGlzcGxheUVsLnN0eWxlLmJhY2tncm91bmRJbWFnZSA9IFwidXJsKCdhc3NldHMvaW1nL3Jlc3VsdC5wbmcnKVwiO1xyXG5cclxuICAgICAgICBmaWxlRGlzcGxheUVsLmlubmVySFRNTCA9IGBcclxuXHRcdFx0XHRcdDxidXR0b24gY2xhc3M9XCJkZWwtdmV3XCI+PGltZyBzcmM9XCJhc3NldHMvaW1nL2RlbC12aWV3LnBuZ1wiIGFsdD1cIlwiIC8+PC9idXR0b24+XHJcblx0XHRcdFx0XHQ8ZGl2Pi4gJHtmaWxlLm5hbWUuc3BsaXQoJy4nKS5wb3AoKX08ZGl2PlxyXG5cdCAgICBcdGA7XHJcbiAgICAgICAgZmlsZUxpc3REaXNwbGF5LmFwcGVuZENoaWxkKGZpbGVEaXNwbGF5RWwpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGlmKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCA8PSAxMjAwKSB7XHJcbiAgICAgICAgbGV0IGhlaWdodEVsZW1lbnRBZGQgPSBmaWxlTGlzdERpc3BsYXkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0O1xyXG4gICAgICAgIGlmIChmaWxlTGlzdERpc3BsYXkuaGFzQ2hpbGROb2RlcygpKSB7XHJcbiAgICAgICAgICAvLyBib2R5QWRkSGVpZ3RoLnN0eWxlLmhlaWdodCA9IFwiY2FsYyhcIisxMDArXCJ2aCAtIChcIitmb290ZXJNZXNzYWdlSGVpZ2h0ICsgXCJweCArIFwiK2hlYWRlck1lc3NhZ2VIZWFkK1wicHggKyBcIitoZWlnaHRFbGVtZW50QWRkK1wicHggKyBcIisxNStcInB4KSlcIjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG4gIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGUpID0+IHtcclxuICAgIGxldCB0YXJnZXQgPSBlLnRhcmdldDtcclxuXHJcbiAgICBsZXQgZWwgPSB0YXJnZXQuY2xvc2VzdCgnLmNoYXQtdmlldyAuZGVsLXZldycpO1xyXG4gICAgaWYgKCFlbCkgcmV0dXJuO1xyXG5cclxuICAgIGNvbnNvbGUubG9nKGVsLnBhcmVudEVsZW1lbnQucGFyZW50RWxlbWVudClcclxuXHJcbiAgICBlbC5wYXJlbnROb2RlLnJlbW92ZSgpO1xyXG5cclxuICAgIGlmICghZmlsZUxpc3REaXNwbGF5Lmhhc0NoaWxkTm9kZXMoKSkge1xyXG4gICAgICBib2R5QWRkSGVpZ3RoLnN0eWxlLmhlaWdodCA9IFwiY2FsYyhcIisxMDArXCJ2aCAtIChcIitmb290ZXJNZXNzYWdlSGVpZ2h0ICsgXCJweCArIFwiK2hlYWRlck1lc3NhZ2VIZWFkK1wicHggKyBcIisxNStcInB4KSlcIjtcclxuICAgIH1cclxuICB9KTtcclxuXHJcblxyXG59KSgpO1xyXG4vLyAkKHdpbmRvdykub24oXCJsb2FkXCIsIGZ1bmN0aW9uICgpIHtcclxuLy8gICAkKFwiLmxpc3QtdG92YXIsIHJlZ2lvbi1zZWxlY3QsIC5zY3JvbGwtZGVmYXVsdCwgLnNjci1lbFwiKS5tQ3VzdG9tU2Nyb2xsYmFyKHtcclxuLy8gICAgIGF4aXM6IFwieVwiLFxyXG4vLyAgICAgdGhlbWU6IFwiY3VzdG9tLWVsXCIsXHJcbi8vICAgICBvblRvdGFsU2Nyb2xsT2Zmc2V0OiA2MCxcclxuLy8gICAgIGNhbGxiYWNrczoge1xyXG4vLyAgICAgICBvbkNyZWF0ZTogZnVuY3Rpb24gKCkge1xyXG4vLyAgICAgICAgICQodGhpcykuZmluZChcIi5pdGVtXCIpLmNzcyhcIndpZHRoXCIsICQodGhpcykud2lkdGgoKSk7XHJcbi8vICAgICAgIH0sXHJcbi8vICAgICAgIG9uQmVmb3JlVXBkYXRlOiBmdW5jdGlvbiAoKSB7XHJcbi8vICAgICAgICAgJCh0aGlzKS5maW5kKFwiLml0ZW1cIikuY3NzKFwid2lkdGhcIiwgJCh0aGlzKS53aWR0aCgpKTtcclxuLy8gICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyAgIH0pO1xyXG4vLyB9KTtcclxuXHJcbi8vIChmdW5jdGlvbiAoKSB7XHJcbi8vXHJcbi8vXHJcbi8vXHJcbi8vXHJcbi8vXHJcbi8vXHJcbi8vICAgaWYgKCQoJy5vYmplY3RfX2JveCcpLmxlbmd0aCA+IDMpIHtcclxuLy8gICAgICQoJy5vYmplY3QtLXNsaWRlcicpLnNsaWNrKHtcclxuLy8gICAgICAgZG90czogZmFsc2UsXHJcbi8vICAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuLy8gICAgICAgc2xpZGVzVG9TY3JvbGw6IDMsXHJcbi8vICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuLy8gICAgICAgc3BlZWQ6IDMwMCxcclxuLy8gICAgICAgbmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgYXJpYS1sYWJlbD1cItCh0LvQtdC00YPRjtGJ0LjQuVwiIGNsYXNzPVwic2xpY2stbmV4dFwiPjxzdmcgd2lkdGg9XCIxOFwiIGhlaWdodD1cIjMyXCIgdmlld0JveD1cIjAgMCAxOCAzMlwiIGZpbGw9XCJub25lXCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPjxwYXRoIGQ9XCJNMiAxTDE1IDE2LjQzMTJMMiAzMFwiIHN0cm9rZT1cIiNBRUE2OURcIiBzdHJva2Utb3BhY2l0eT1cIjAuM1wiIHN0cm9rZS13aWR0aD1cIjNcIi8+PC9zdmc+PC9idXR0b24+JyxcclxuLy8gICAgICAgcHJldkFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgYXJpYS1sYWJlbD1cItCf0YDQtdC00YvQtNGD0YnQuNC5XCIgY2xhc3M9XCJzbGljay1sZWZ0XCI+PHN2ZyB3aWR0aD1cIjE4XCIgaGVpZ2h0PVwiMzJcIiB2aWV3Qm94PVwiMCAwIDE4IDMyXCIgZmlsbD1cIm5vbmVcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+PHBhdGggZD1cIk0xNiAzMUwzIDE1LjU2ODhMMTYgMlwiIHN0cm9rZT1cIiNBRUE2OURcIiBzdHJva2Utb3BhY2l0eT1cIjAuM1wiIHN0cm9rZS13aWR0aD1cIjNcIi8+PC9zdmc+PC9idXR0b24+JyxcclxuLy8gICAgICAgcmVzcG9uc2l2ZTogW1xyXG4vLyAgICAgICAgIHtcclxuLy8gICAgICAgICAgIGJyZWFrcG9pbnQ6IDEyMDAsXHJcbi8vICAgICAgICAgICBzZXR0aW5nczoge1xyXG4vLyAgICAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxyXG4vLyAgICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4vLyAgICAgICAgICAgfVxyXG4vLyAgICAgICAgIH0sXHJcbi8vICAgICAgICAge1xyXG4vLyAgICAgICAgICAgYnJlYWtwb2ludDogOTkyLFxyXG4vLyAgICAgICAgICAgc2V0dGluZ3M6IHtcclxuLy8gICAgICAgICAgICAgdmFyaWFibGVXaWR0aDogdHJ1ZSxcclxuLy8gICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDIsXHJcbi8vICAgICAgICAgICB9XHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgXVxyXG4vLyAgICAgfSk7XHJcbi8vICAgfVxyXG4vL1xyXG4vLyAgICQoJy5jb2xsYXBzZS1lbGVtZW50cyAubGluay1vcGVuJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbi8vICAgICBsZXQgZHJvcERvd24gPSAkKHRoaXMpLmNsb3Nlc3QoJy5jb2xsYXBzZS1lbGVtZW50JykuZmluZCgnLmNvbGxhcHNlLWVsZW1lbnQtYm9keScpO1xyXG4vLyAgICAgJCh0aGlzKS5jbG9zZXN0KCcuY29sbGFwc2UtZWxlbWVudHMnKS5maW5kKCcuY29sbGFwc2UtZWxlbWVudC1ib2R5Jykubm90KGRyb3BEb3duKS5zbGlkZVVwKCk7XHJcbi8vICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xyXG4vLyAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4vLyAgICAgfSBlbHNlIHtcclxuLy8gICAgICAgJCh0aGlzKS5jbG9zZXN0KCcuY29sbGFwc2UtZWxlbWVudCcpLmZpbmQoJy5saW5rLW9wZW4uYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4vLyAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuLy8gICAgIH1cclxuLy8gICAgIGRyb3BEb3duLnN0b3AoZmFsc2UsIHRydWUpLnNsaWRlVG9nZ2xlKCk7XHJcbi8vICAgfSk7XHJcbi8vXHJcbi8vICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5oaWRlLWJ1dHRvbicsIGZ1bmN0aW9uIChlKSB7XHJcbi8vICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbi8vICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdhY3RpdmUtaGlkZScpO1xyXG4vLyAgICAgaWYgKCQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZS1oaWRlJykpIHtcclxuLy8gICAgICAgJCh0aGlzKS5wYXJlbnRzKCcuZmVlZGJhY2tfX2l0ZW0nKS5maW5kKCcuZmVlZGJhY2tfX2JvZHknKS5zbGlkZVRvZ2dsZSgpO1xyXG4vLyAgICAgICAkKHRoaXMpLnRleHQoJ9Cf0L7QutCw0LfQsNGC0Ywg0L7RgtC30YvQstGLJyk7XHJcbi8vICAgICAgICQodGhpcykucGFyZW50cygnLmZlZWRiYWNrX19pdGVtJykuZmluZCgnLmZlZWRiYWNrX19ib2R5JykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuLy8gICAgIH0gZWxzZSB7XHJcbi8vICAgICAgICQodGhpcykucGFyZW50cygnLmZlZWRiYWNrX19pdGVtJykuZmluZCgnLmZlZWRiYWNrX19ib2R5Jykuc2xpZGVUb2dnbGUoKTtcclxuLy8gICAgICAgJCh0aGlzKS5wYXJlbnRzKCcuZmVlZGJhY2tfX2l0ZW0nKS5maW5kKCcuZmVlZGJhY2tfX2JvZHknKS5hZGRDbGFzcygnaGlkZScpO1xyXG4vLyAgICAgICAkKHRoaXMpLnRleHQoJ9Ch0LrRgNGL0YLRjCDQvtGC0LfRi9Cy0YsnKTtcclxuLy8gICAgIH1cclxuLy8gICB9KTtcclxuLy9cclxuLy8gICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgpO1xyXG4vLyAgICQoJy5kcmFnQW5kcm9wJykuc29ydGFibGUoe1xyXG4vLyAgICAgaXRlbXM6ICc+IC5jYXRlZ29yeS1lZGl0JyxcclxuLy8gICAgIGZvcmNlUGxhY2Vob2xkZXJTaXplOiB0cnVlLFxyXG4vLyAgICAgaGFuZGxlOiAnLmhhbmRsZScsXHJcbi8vICAgICBwbGFjZWhvbGRlcjogJ3NvcnRhYmxlLXBsYWNlaG9sZGVyJyxcclxuLy8gICAgIHN0YXJ0OiBmdW5jdGlvbiAoZSwgdWkpIHtcclxuLy8gICAgICAgdWkucGxhY2Vob2xkZXIuaGVpZ2h0KHVpLml0ZW0uaGVpZ2h0KCkpO1xyXG4vLyAgICAgfSxcclxuLy8gICB9KTtcclxuLy8gICAkKFwiLmRyYWdBbmRyb3BcIikuZGlzYWJsZVNlbGVjdGlvbigpO1xyXG4vLyB9KTtcclxuLy9cclxuLy8gLy8gKGZ1bmN0aW9uICgpIHtcclxuLy8gLy9cclxuLy8gLy8gICAvLyAg0LzQvtC00LDQu9C60LhcclxuLy8gLy8gICAkKGRvY3VtZW50KS5vbignc2hvdy5icy5tb2RhbCcsICcubW9kYWwnLCBmdW5jdGlvbiAoKSB7XHJcbi8vIC8vICAgICBsZXQgekluZGV4ID0gMTA0MCArICgxMCAqICQoJy5tb2RhbDp2aXNpYmxlJykubGVuZ3RoKTtcclxuLy8gLy8gICAgICQodGhpcykuY3NzKCd6LWluZGV4JywgekluZGV4KTtcclxuLy8gLy8gICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4vLyAvLyAgICAgICAkKCcubW9kYWwtYmFja2Ryb3AnKS5ub3QoJy5tb2RhbC1zdGFjaycpLmNzcygnei1pbmRleCcsIHpJbmRleCAtIDEpLmFkZENsYXNzKCdtb2RhbC1zdGFjaycpO1xyXG4vLyAvLyAgICAgfSwgMCk7XHJcbi8vIC8vICAgfSk7XHJcbi8vIC8vXHJcbi8vIC8vICAgLy/RhNC40LvRjNGC0YAgYnRuXHJcbi8vIC8vIFx0bGV0IGZpbHRlckJ0biA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250cm9scycpO1xyXG4vLyAvLyBcdGlmIChmaWx0ZXJCdG4pIHtcclxuLy8gLy8gXHRcdGZpbHRlckJ0bi5vbmNsaWNrID0gZnVuY3Rpb24oZSkge1xyXG4vLyAvLyBcdFx0XHRsZXQgZmlsdGVyQnRuID0gZS50YXJnZXQuY2xvc2VzdCgnLmZpbHRlci1idXR0b24nKTtcclxuLy8gLy9cclxuLy8gLy8gXHRcdFx0aWYgKCFmaWx0ZXJCdG4pIHJldHVybjtcclxuLy8gLy8gXHRcdFx0ZmlsdGVyQnRuLm5leHRFbGVtZW50U2libGluZy5uZXh0RWxlbWVudFNpYmxpbmcuY2xhc3NMaXN0LnRvZ2dsZSgnYWN0aXZlJylcclxuLy8gLy8gXHRcdH1cclxuLy8gLy8gXHR9XHJcbi8vIC8vXHJcbi8vIC8vICAgJChkb2N1bWVudCkub24oJ2hpZGRlbi5icy5tb2RhbCcsICcubW9kYWwnLCBmdW5jdGlvbiAoKSB7XHJcbi8vIC8vICAgICAkKCcubW9kYWw6dmlzaWJsZScpLmxlbmd0aCAmJiAkKGRvY3VtZW50LmJvZHkpLmFkZENsYXNzKCdtb2RhbC1vcGVuJyk7XHJcbi8vIC8vICAgfSk7XHJcbi8vIC8vXHJcbi8vIC8vICAgJCgnLmduci10cmVlX19uYW1lJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbi8vIC8vIFx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcygnYWN0aXZlJykpIHtcclxuLy8gLy8gXHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbi8vIC8vIFx0XHRcdCQodGhpcykucGFyZW50KCkuY2hpbGRyZW4oJy5nbnItdHJlZV9fZ3JvdXAnKS5zbGlkZVVwKDIwMCk7XHJcbi8vIC8vIFx0XHRcdC8vICQodGhpcykuY2hpbGRyZW4oKS5zbGlkZVVwKCk7XHJcbi8vIC8vIFx0XHR9IGVsc2Uge1xyXG4vLyAvLyBcdFx0XHQkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuLy8gLy8gXHRcdFx0JCh0aGlzKS5wYXJlbnQoKS5jaGlsZHJlbignLmduci10cmVlX19ncm91cCcpLnNsaWRlRG93bigyMDApO1xyXG4vLyAvLyBcdFx0fVxyXG4vLyAvLyBcdH0pO1xyXG4vLyAvL1xyXG4vLyAvLyAgICQoJy5mbGV4LW1lbnUnKS5mbGV4TWVudSh7XHJcbi8vIC8vIFx0XHRjdXRvZmY6IDIsXHJcbi8vIC8vIFx0XHRzaG93T25Ib3ZlcjogZmFsc2UsXHJcbi8vIC8vIFx0XHQvLyBzaG93T25Ib3ZlcjogZmFsc2UsXHJcbi8vIC8vIFx0XHRsaW5rVGV4dDogXCI8aSBjbGFzcz0nbXktaWNvbic+PC9pPlwiLFxyXG4vLyAvLyBcdFx0cG9wdXBDbGFzczogJ21vcmVfZHJvcGRvd24nLFxyXG4vLyAvLyBcdFx0bGlua1RleHRBbGw6IFwi0JzQtdC90Y5cIixcclxuLy8gLy8gXHR9KTtcclxuLy8gLy9cclxuLy8gLy8gICAvLyDRgdC70LDQudC00LXRgCDQtNC70Y8g0LPQu9Cw0LLQvdC+0Lkg0YHRgtGA0LDQvdC40YbRi1xyXG4vLyAvLyBcdGlmICgkKHdpbmRvdykud2lkdGgoKSA8IDc2OCkge1xyXG4vLyAvLyBcdFx0JCgnLnNlY3Rpb24tc3RlcCAucm93Jykuc2xpY2soe1xyXG4vLyAvLyBcdFx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuLy8gLy8gXHRcdFx0c2xpZGVzVG9TaG93OiAxLFxyXG4vLyAvLyBcdFx0XHRkb3RzOiB0cnVlLFxyXG4vLyAvLyBcdFx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuLy8gLy8gXHRcdFx0c3BlZWQ6IDMwMCxcclxuLy8gLy8gXHRcdFx0YXJyb3dzOiBmYWxzZSxcclxuLy8gLy8gXHRcdFx0YWRhcHRpdmVIZWlnaHQ6IHRydWUsXHJcbi8vIC8vIFx0XHRcdGZhZGU6IHRydWUsXHJcbi8vIC8vIFx0XHRcdHJlc3BvbnNpdmU6IFtcclxuLy8gLy8gXHRcdFx0XHR7XHJcbi8vIC8vIFx0XHRcdFx0XHRicmVha3BvaW50OiA0ODAsXHJcbi8vIC8vIFx0XHRcdFx0XHRzZXR0aW5nczoge1xyXG4vLyAvLyBcdFx0XHRcdFx0XHRmYWRlOiBmYWxzZVxyXG4vLyAvLyBcdFx0XHRcdFx0fVxyXG4vLyAvLyBcdFx0XHRcdH1cclxuLy8gLy8gXHRcdFx0XVxyXG4vLyAvLyBcdFx0fSk7XHJcbi8vIC8vIFx0fVxyXG4vLyAvLyB9KTtcclxuLy8gLy9cclxuLy8gLy9cclxuIl19
