'use strict';

module.exports = function() {
  $.gulp.task('scriptsLint', function() {
    return $.gulp.src(['./build/js/*.js'])
      .pipe($.plumber())
      .pipe($.eslint())
      .pipe($.eslint.format())
      .pipe($.eslint.failAfterError());
  })
};
