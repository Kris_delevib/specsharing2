'use strict';

module.exports = [
  './node_modules/jquery/dist/jquery.min.js',
  './source/libs/jquery-ui/jquery-ui.min.js',
  './node_modules/popper.js/dist/umd/popper.min.js',
  // './node_modules/moment/min/moment.min.js',
  // './node_modules/moment/min/moment-with-locales.min.js',
  './node_modules/bootstrap/dist/js/bootstrap.min.js',
  './node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
  './node_modules/bootstrap-select/dist/js/i18n/defaults-ru_RU.min.js',
  './node_modules/slick-carousel/slick/slick.min.js',
  './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
  './source/libs/flexmenu.min.js',
  './source/libs/notify.js',
  './node_modules/air-datepicker/dist/js/datepicker.min.js',
  './source/libs/timepicker/jquery.timepicker.min.js',
];
