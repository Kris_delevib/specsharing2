$('#test1').datepicker({
  // Можно выбрать тольо даты, идущие за сегодняшним днем, включая сегодня
  minDate: new Date(),
  autoClose: false,
});


$('#test-timepicker').timepicker({
  timeFormat: 'H:mm',
  interval: 30,
  defaultTime: '8',
  dynamic: false,
  scrollbar: true,
});

// //
// // $('.modal').on('scroll', function(e) {
// //   if ($('.datepicker').hasClass('active')) {
// //     $('.datepicker').removeClass('active')
// //   }
// // })
// //
// // // function createElement(tag, props, ...children) {
// // //   const element = document.createElement(tag);
// // //
// // //   if (props) {
// // //     Object.entries(props).forEach(([key, value]) => {
// // //       if (key.startsWith('on') && typeof value === 'function') {
// // //         element.addEventListener(key.substring(2), value);
// // //       } else if (key.startsWith('data-')) {
// // //         element.setAttribute(key, value);
// // //       } else {
// // //         element[key] = value;
// // //       }
// // //     });
// // //   }
// // //
// // //   children.forEach(child => {
// // //     if (Array.isArray(child)) {
// // //       return element.append(...child);
// // //     }
// // //
// // //     if (typeof child === 'string' || typeof child === 'number') {
// // //       child = document.createTextNode(child);
// // //     }
// // //
// // //     if (child instanceof Node) {
// // //       element.appendChild(child);
// // //     }
// // //   });
// // //
// // //   return element;
// // // }
// // //
// // // let dateOne = document.querySelector('#test1');
// // //
// // // if (dateOne) {
// // //   class Calendar {
// // //     constructor(element, obj) {
// // //
// // //       this.parentEl = $('body');
// // //       this.element = $(element);
// // //       this.startDate = moment().startOf('day');
// // //       this.endDate = moment().endOf('day');
// // //
// // //       this.minDate = false;
// // //       this.maxSpan = false;
// // //       this.minYear = moment().clone().subtract(1, 'year').format('YYYY');
// // //       this.maxYear = moment().clone().add(1, 'year').format('YYYY');
// // //       this.leftCalendar = {};
// // //       this.rightCalendar = {};
// // //       this.isShowing = false;
// // //
// // //       // один календарь
// // //       this.singleCalendar = false;
// // //
// // //       if (typeof obj !== 'object' || obj === null) {
// // //         obj = {};
// // //       }
// // //
// // //       this.template = createElement('div', { className: 'calendar' },
// // //         createElement('div', { className: "calendar-inner" },
// // //           createElement('div', { className: 'calendar-drp left' },
// // //             createElement('div', { className: 'calendar-head' },),
// // //             createElement('div', {className: 'calendar-table'}),
// // //           ),
// // //           createElement('div', {className: 'calendar-drp right'},
// // //             createElement('div', {className: 'calendar-head'},
// // //               createElement('div', { className: 'calendar-head-wrap calendar-head-wrap-option'})
// // //             ),
// // //             createElement('div', {
// // //               className: 'calendar-table'
// // //             }),
// // //           ),
// // //         )
// // //       );
// // //
// // //       this.clickDate = this.clickDate.bind(this);
// // //       this.show = this.show.bind(this);
// // //       this.toggle = this.toggle.bind(this);
// // //       this.keydown = this.keydown.bind(this);
// // //       this.outsideClick = this.outsideClick.bind(this);
// // //       this.clickPrev = this.clickPrev.bind(this);
// // //       this.clickNext = this.clickNext.bind(this);
// // //
// // //
// // //       this.locale = {
// // //         daysOfWeek:  moment.localeData('ru').weekdaysMin(),
// // //         monthNames: moment.localeData('ru').months(),
// // //       };
// // //
// // //
// // //       let insert = this.element.parent();
// // //       this.container = $(this.template).appendTo(insert);
// // //
// // //       this.container.find('.calendar-drp')
// // //         .on('mousedown.calendar', 'td.available', this.clickDate)
// // //         .on('click.calendar', '.prev', this.clickPrev)
// // //         .on('click.calendar', '.next ', this.clickNext);
// // //
// // //
// // //       if (typeof obj.singleCalendar === 'boolean') {
// // //         this.singleCalendar = obj.singleCalendar;
// // //
// // //         if (this.singleCalendar) {
// // //           this.endDate = this.startDate.clone()
// // //         }
// // //       }
// // //
// // //       if (this.singleCalendar) {
// // //         this.container.find('.calendar-drp.right').hide();
// // //       }
// // //
// // //       // значение из input
// // //       let start, end;
// // //
// // //       if ($(this.element).is(':text')) {
// // //
// // //         let val = $(this.element).val(),
// // //           split = val.split(' - ');
// // //
// // //         start = end = null;
// // //
// // //         if (split.length == 2) {
// // //           start = moment(split[0], 'DD.MM.YYYY').clone();
// // //           end = 	moment(split[1], 'DD.MM.YYYY').clone()
// // //         } else if (this.singleCalendar && val !== '') {
// // //           start = moment(val, 'DD.MM.YYYY');
// // //           end = moment(val, 'DD.MM.YYYY');
// // //         }
// // //
// // //         if (start !== null && end !== null) {
// // //           this.setStartDate(start);
// // //           this.setEndDate(end);
// // //         }
// // //       }
// // //
// // //       if (this.element.is("input")) {
// // //         this.element.on({
// // //           'click.calendar': this.show,
// // //           'focus.calendar': this.show,
// // //           'keydown.calendar': this.keydown
// // //         })
// // //       } else {
// // //         this.element.on('click.calendar', this.toggle)
// // //         this.element.on('keydown.calendar', this.toggle)
// // //       }
// // //
// // //       this.updateElement();
// // //     }
// // //
// // //     setStartDate(startDate) {
// // //       if (typeof startDate === 'string') {
// // //         this.startDate = moment(startDate, moment.format('DD.MM.YYYY HH:mm'));
// // //       }
// // //
// // //       if (typeof startDate === 'object') {
// // //         this.startDate = moment(startDate);
// // //       }
// // //
// // //       this.updateMonthsInView();
// // //
// // //     }
// // //
// // //     setEndDate (endDate) {
// // //       if (typeof endDate === 'string') {
// // //         this.endDate = moment(startDate, moment.format('DD.MM.YYYY HH:mm'));
// // //       }
// // //
// // //       if (typeof endDate === 'object') {
// // //         this.endDate = moment(endDate);
// // //       }
// // //
// // //       if (this.endDate.isBefore(this.startDate)) {
// // //         this.endDate = this.startDate.clone();
// // //       }
// // //
// // //       if (this.maxSpan && this.startDate.clone().add(this.maxSpan).isBefore(this.endDate)) {
// // //         this.endDate = this.startDate.clone().add(this.maxSpan);
// // //       }
// // //
// // //       this.updateMonthsInView();
// // //     }
// // //
// // //     isInvalidDate() {
// // //       return false;
// // //     }
// // //
// // //     isCustomDate() {
// // //       return false;
// // //     }
// // //
// // //     updateView() {
// // //       this.updateMonthsInView();
// // //       this.updateCalendars();
// // //     }
// // //
// // //     updateMonthsInView() {
// // //       if (this.endDate) {
// // //         this.leftCalendar.month = this.startDate.clone().date(2);
// // //
// // //         if (this.endDate.month() != this.startDate.month() || this.endDate.year() != this.startDate.year()) {
// // //           this.rightCalendar.month = this.endDate.clone().date(2);
// // //         } else {
// // //           this.rightCalendar.month = this.startDate.clone().date(2).add(1, 'month');
// // //         }
// // //       }
// // //     }
// // //
// // //     updateCalendars() {
// // //       this.renderCalendar('left');
// // //       this.renderCalendar('right');
// // //     }
// // //
// // //     renderCalendar(side) {
// // //       var calendar = side == 'left' ? this.leftCalendar : this.rightCalendar;
// // //
// // //       var month = calendar.month.month();
// // //       var year = calendar.month.year();
// // //
// // //       var daysInMonth = moment([year, month]).daysInMonth();
// // //       var firstDay = moment([year, month, 1]);
// // //       var lastDay = moment([year, month, daysInMonth]);
// // //       var lastMonth = moment(firstDay).subtract(1, 'month').month();
// // //       var lastYear = moment(firstDay).subtract(1, 'month').year();
// // //       var daysInLastMonth = moment([lastYear, lastMonth]).daysInMonth();
// // //       var dayOfWeek = firstDay.day();
// // //
// // //       var hour = calendar.month.hour();
// // //       var minute = calendar.month.minute();
// // //
// // //       var calendar = [];
// // //       calendar.firstDay = firstDay;
// // //       calendar.lastDay = lastDay;
// // //
// // //       for (var i = 0; i < 6; i++) {
// // //         calendar[i] = [];
// // //       }
// // //
// // //       var startDay = daysInLastMonth - dayOfWeek + moment.localeData('ru').firstDayOfWeek() + 1;
// // //       if (startDay > daysInLastMonth)
// // //         startDay -= 7;
// // //
// // //       if (dayOfWeek == moment.localeData('ru').firstDayOfWeek())
// // //         startDay = daysInLastMonth - 6;
// // //
// // //       var curDate = moment([lastYear, lastMonth, startDay, 12, minute]);
// // //
// // //       var col, row;
// // //       for (var i = 0, col = 0, row = 0; i < 42; i++, col++, curDate = moment(curDate).add(24, 'hour')) {
// // //         if (i > 0 && col % 7 === 0) {
// // //           col = 0;
// // //           row++;
// // //         }
// // //         calendar[row][col] = curDate.clone().hour(hour).minute(minute);
// // //         curDate.hour(12);
// // //       }
// // //
// // //       // make the calendar object available to hoverDate/clickDate
// // //       if (side == 'left') {
// // //         this.leftCalendar.calendar = calendar;
// // //       } else {
// // //         this.rightCalendar.calendar = calendar;
// // //       }
// // //
// // //       //выбор времени и даты
// // //       var currentMonth = calendar[1][1].month();
// // //       console.log(currentMonth)
// // //       var currentYear = calendar[1][1].year();
// // //
// // //       let minDate = side == 'left' ?false:this.startDate;
// // //
// // //       var maxYear = this.maxYear;
// // //       var minYear = (minDate && minDate.year()) || (this.minYear);
// // //       var inMinYear = currentYear == minYear;
// // //       var inMaxYear = currentYear == maxYear;
// // //
// // //
// // //
// // //
// // //       var nameMonth = moment.localeData('ru').months();
// // //
// // //       var monthHtml = "<div class='calendar-head-wrap'>";
// // //       monthHtml += '<button class="prev available"><span><</span></button>';
// // //
// // //       var dateHtml = this.locale.monthNames[calendar[1][1].month()] + calendar[1][1].format(" YYYY");
// // //
// // //       monthHtml += '<div class="calendar-caption">' + dateHtml + '</div>';
// // //
// // //       monthHtml += '<button class="next available"><span>></span></button>';
// // //
// // //       this.container.find('.calendar-drp.' + side + ' .calendar-head').html(monthHtml);
// // //
// // //       var html = '<table>';
// // //       html += '<thead>';
// // //       html += '<tr>';
// // //       $.each(this.locale.daysOfWeek, function(index, dayOfWeek) {
// // //         html += '<th>' + dayOfWeek + '</th>';
// // //       });
// // //       html += '<tr>';
// // //       html += '</thead>';
// // //       html += '<tbody>';
// // //
// // //       for (let row = 0; row < 6; row++) {
// // //         html += '<tr>';
// // //
// // //         // add week number
// // //         for (let col = 0; col < 7; col++) {
// // //           let classes = [];
// // //
// // //           // активный
// // //           if (calendar[row][col].format('DD.MM.YYYY') == this.startDate.format('DD.MM.YYYY')) {
// // //             classes.push('active', 'start-date');
// // //           }
// // //
// // //           //today
// // //           if (calendar[row][col].isSame(new Date(), "day"))
// // //             classes.push('today');
// // //
// // //           //highlight the currently selected end date
// // //           if (this.endDate != null && calendar[row][col].format('DD.MM.YYYY') == this.endDate.format('DD.MM.YYYY'))
// // //             classes.push('active', 'end-date');
// // //
// // //
// // //           // weekends
// // //           if (calendar[row][col].isoWeekday() > 5)
// // //             classes.push('weekend');
// // //
// // //           //highlight dates in-between the selected dates
// // //           if (this.endDate != null && calendar[row][col] > this.startDate && calendar[row][col] < this.endDate)
// // //             classes.push('in-range');
// // //
// // //
// // //           //grey out the dates in other months displayed at beginning and end of this calendar
// // //           if (calendar[row][col].month() != calendar[1][1].month())
// // //             classes.push('off', 'ends');
// // //
// // //           //don't allow selection of date if a custom function decides it's invalid
// // //           if (this.isInvalidDate(calendar[row][col]))
// // //             classes.push('off', 'disabled');
// // //
// // //
// // //           //apply custom classes for this date
// // //           var isCustom = this.isCustomDate(calendar[row][col]);
// // //           if (isCustom !== false) {
// // //             if (typeof isCustom === 'string')
// // //               classes.push(isCustom);
// // //             else
// // //               Array.prototype.push.apply(classes, isCustom);
// // //           }
// // //
// // //
// // //           var cname = '', disabled = false;
// // //           for (var i = 0; i < classes.length; i++) {
// // //             cname += classes[i] + ' ';
// // //             if (classes[i] == 'disabled')
// // //               disabled = true;
// // //           }
// // //           if (!disabled)
// // //             cname += 'available';
// // //           html += '<td class="' + cname.replace(/^\s+|\s+$/g, '') + '" data-title="' + 'r' + row + 'c' + col + '">' + calendar[row][col].date() + '</td>';
// // //         }
// // //         html += '</tr>';
// // //       }
// // //       html += '</tbody>';
// // //       html += '</table>';
// // //
// // //       this.container.find('.calendar-drp.' + side + ' .calendar-table').html(html);
// // //     }
// // //
// // //     move() {
// // //       this.container.css({
// // //         top: 80,
// // //         left: 0,
// // //         right: 'auto',
// // //         visibility: 'visible',
// // //         opacity: 1
// // //       });
// // //     }
// // //
// // //     show() {
// // //       if (this.isShowing) return;
// // //
// // //       $(document)
// // //         .on('mousedown', this.outsideClick)
// // //         //mob
// // //         .on('touchend', this.outsideClick)
// // //         .on('focusin', this.outsideClick);
// // //
// // //       this.updateView();
// // //       this.container.show();
// // //       this.move();
// // //       this.element.trigger('show', this);
// // //       this.isShowing = true;
// // //     }
// // //
// // //     hide() {
// // //       if (!this.isShowing) return;
// // //
// // //       if (!this.endDate) {
// // //         this.startDate = this.oldStartDate.clone();
// // //         this.endDate = this.oldEndDate.clone();
// // //       }
// // //
// // //       this.updateElement();
// // //
// // //       $(document).off('.calendar');
// // //       $(window).off('.calendar');
// // //       this.container.hide();
// // //       this.element.trigger('hide.calendar', this);
// // //       this.isShowing = false;
// // //     }
// // //
// // //     outsideClick(e) {
// // //       let target = $(e.target);
// // //
// // //       if (target.closest(this.element).length || target.closest(this.container).length || target.closest('.calendar-inner').length) return
// // //       this.hide();
// // //     }
// // //
// // //     toggle() {
// // //       if (this.isShowing) {
// // //         this.hide();
// // //       } else {
// // //         this.show();
// // //       }
// // //     }
// // //
// // //     clickPrev() {
// // //       this.leftCalendar.month.subtract(1, 'month');
// // //       this.updateCalendars();
// // //     }
// // //
// // //     clickNext() {
// // //       this.leftCalendar.month.add(1, 'month');
// // //       this.updateCalendars();
// // //     }
// // //
// // //     clickDate(e) {
// // //       let title = $(e.target).attr('data-title');
// // //       let row = title.substr(1, 1);
// // //       let col = title.substr(3, 1);
// // //       let cal = $(e.target).parents('.calendar-drp');
// // //       let date = cal.hasClass('left') ? this.leftCalendar.calendar[row][col] : this.rightCalendar.calendar[row][col];
// // //
// // //       if (this.endDate || date.isBefore(this.startDate, 'day')) {
// // //         this.endDate = null;
// // //         this.setStartDate(date.clone());
// // //       }
// // //
// // //       if (this.singleCalendar) {
// // //         this.setEndDate(this.startDate);
// // //       }
// // //
// // //       this.updateView();
// // //       e.stopPropagation();
// // //     }
// // //
// // //     keydown(e) {
// // //       // 9 - tab, 27 - esc, 27 - enter
// // //       if ((e.keyCode === 9) || (e.keyCode === 13)) {
// // //         this.hide();
// // //       }
// // //
// // //       if (e.keyCode === 27) {
// // //         e.preventDefault();
// // //         e.stopPropagation();
// // //         this.hide();
// // //       }
// // //     }
// // //
// // //     updateElement() {
// // //       if (this.element.is('input')) {
// // //         let newValue = this.startDate.format('DD.MM.YYYY HH:mm');
// // //
// // //         if (newValue !== this.element.val()) {
// // //           this.element.val(newValue).trigger('change');
// // //         }
// // //       }
// // //     }
// // //
// // //     remove() {
// // //       this.container.remove();
// // //     }
// // //   }
// // //   new Calendar(dateOne, {
// // //     singleCalendar: true
// // //   });
// // // }