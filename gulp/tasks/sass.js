'use strict';

module.exports = function() {

  $.gulp.task('sass', function() {
    return $.gulp.src([
			'./source/style/critical.scss',
			'./source/style/app.scss'
			])
      .pipe($.plumber())
      .pipe($.sass()).on('error', $.notify.onError({ title: 'Style' }))
      .pipe($.postcss([$.autoprefixer(), $.cssnano()]))
      .pipe($.cssUnit({
      	type     :    'px-to-rem',
      	rootSize :    16
      }))
      // .pipe($.sourcemaps.write())
      .pipe($.gulp.dest($.config.root + '/assets/css'))
      .pipe($.browserSync.stream());
  })

  $.gulp.task('display-sass', function() {
    return $.gulp.src('./source/style/display.scss')
      .pipe($.plumber())
      .pipe($.sass()).on('error', $.notify.onError({ title: 'Style' }))
      // .pipe($.uncss({
      //   html: ['build/*.html']
      // }))
      .pipe($.postcss([$.autoprefixer()]))
      .pipe($.cssUnit({
      	type     :    'px-to-rem',
      	rootSize :    16
      }))
      .pipe($.gulp.dest($.config.root + '/assets/css'))
      .pipe($.browserSync.stream());
  })
};

    // return $.gulp.src('./source/style/**/*.scss')