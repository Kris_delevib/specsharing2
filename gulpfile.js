'use strict';

global.$ = {
  package: require('./package.json'),
  config: require('./gulp/config'),
  path: {
    task: require('./gulp/paths/tasks.js'),
    jsFoundation: require('./gulp/paths/js.foundation.js'),
    cssFoundation: require('./gulp/paths/css.foundation.js'),
    app: require('./gulp/paths/app.js')
  },
  gulp: require('gulp'),
  del: require('del'),
  pug: require('gulp-pug'),
  notify: require("gulp-notify"),
  plumber: require('gulp-plumber'),
  imagemin: require('gulp-imagemin'),
  sourcemaps: require('gulp-sourcemaps'),
  sass: require("gulp-sass"),
  cssUnit: require('gulp-css-unit'),
  browserSync: require('browser-sync').create(),
  concat: require('gulp-concat'),
  uglify: require('gulp-uglify'),
  cleanCss: require('gulp-clean-css'),
  eslint: require('gulp-eslint'), 
  concatCss: require('gulp-concat-css'),
  merge: require('merge-stream'),

  uncss: require('gulp-uncss'),

  postcss: require('gulp-postcss'),
  autoprefixer: require('autoprefixer'),
  syntax_scss: require('postcss-scss'),

  stylelint: require("stylelint"),

  cssnano : require('cssnano'),
  // babel: require('gulp-babel')
};


$.path.task.forEach(function(taskPath) {
  require(taskPath)();
});

$.gulp.task('default', $.gulp.series(
  'clean',
  $.gulp.parallel(
    'sass',
    'pug',
    'js:foundation',
    'scripts',
    'images',
    'css:foundation',
    'copy:fonts',
    'display-sass',
  ),
  $.gulp.parallel(
    'watch',
    'serve'
  )
));