'use strict';

module.exports = [
  './source/libs/bootstrap-reboot.min.css',
  './node_modules/bootstrap/dist/css/bootstrap.css',
  './node_modules/slick-carousel/slick/slick.css',
  './node_modules/hamburgers/dist/hamburgers.css',
  './node_modules/bootstrap-select/dist/css/bootstrap-select.min.css',
  './source/libs/jquery-ui/jquery-ui.min.css',
  './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
  './node_modules/air-datepicker/dist/css/datepicker.min.css',
];
