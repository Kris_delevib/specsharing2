# Заказчики
```text
	Исполнители - landlords !

	Заявки(раскрытая) - applications !*
	Заявка выбранный исполнитель - applications-inside !*


	Акты и отчеты - acts !
	Сообщения - message!
	Сообщения.Обратиться в сервис - message-sever!
	Сообщения.Вид для пользователя  - message-inside !
	Баланс - balance !
```


# Заказчики.Заказы
```text
	Заказчик.Карточный вид - order
	Заказчик.Мои заказы.В споре - order-dispute(адпаптив календаря с таба "Месяц")
	Заказчик.Мои заказы.Будущие - order-future(адпаптив календаря с таба "Месяц")
```


# Исполнители
```text
	Каталог заявок в системе -  orders-platform !
	Каталог заявок в системе. Подробнее  - orders-platform-more
	
	Заявки - applications-two
	
	Техника и команда
	  Техника позиция - technique
	  Техника подробнее - technique-more

	---------------------------------------- Проверить
	ЗАКАЗЫ
		order1
```



# Менеджер платформы

## Заявки
```text
	application-block
	application-block-inside
```

## Заказы!
```text
	Заказчик.Мои заказы.В работе. Карточный вид - order-block
	Заказы. Подробне - order-block-more (АДАПАТИВ)
```

## Техника!
```text
	Редактирование структуры категорий  - personal-area-technique!
	Редактрование товарной позиции - editing-position!
	Список техники в товарной позиции - editing-technique!
	Редактрование базы данных техники - editing-technique-db!
	Вся техника, добавленная пользователями на платформу - all-technique!
	Доп оборудование - add-equipment!
```

## Контент страниц!
```text
	Контент - page-content!
```

## Города и регионы
```text
	Города и регионы - page-city
```

## Пользователи
```text
	Менеджер.Пользователи - manager!
	Менеджер.Пользователи.Подробнее - manager-more
	Менеджер.Пользователи.Заявки пользователя - manager-user-applications

	Заявка раскрытая  - manager-applications-more
	Личный кабинет.Арендодатель.Мои заявки.Товарная позиция -  manager-applications-more-detail !


	Менеджер.Пользователи.Заказы пользователя - manager-orders (КАЛЕНДАРЬ)
	Менеджер.Пользователи. Техника пользователя - manager-equipment
	Менеджер.Пользователи. Техника пользователя - manager-equipment-more
	Менеджер.Пользователи - manager-two.png!
	Менеджер. Основные данные - basic-data!
	Запрос на одобрение техн - technical-request
	Менеджер.Пользователи - manager-last (КАЛЕНДАРЬ)
```

## Модерация 
```text
  moderation (Спросить, как сделать адаптив)
	
	Запрос на одобрение техники - moderation-request	
	Проверка документации - moderation-documentation-check  
    moderation-block (Пользователи, Запросы на индивидуальный договор) **
    moderation-block-2*
	Редактирование БД техники  - moderation-bd **
	Индивидуальный договор - moderation-documentation-check-2 **
    Модерация отзыва -
                  moderation-feedback **
	              moderation-feedback2 **


	<!-- таких страниц в макете не было -->
	Заказчик.Мои заказы.В работе. Карточный вид - orderer
	Редактрования базы данных техники - edit-database -->

## Настройки 
```text
	Настройки-  card-view !*
```

## Отчеты
```text
	Отчеты - reports
```

## Шаблоны
```text
	Список шаблонов - template-list!*
	Шаблон - commodity-position (закончить с адпаптивои)
```



## Сообщения
```text
	Список чатов - chat-list
	Вид для пользователя - user-view
```

## Карточка и корзина
```text
	Детальная - сatalog-detail*
	Детальная 2 - сatalog-detail2 (!*)
	Корзина - basket *
```

## Уведомления и личные данные
```text
	Личные данные - personal-data*
	Личные данные 2 - personal-data-2*
	Уведомления - notifications
```

## Категории
```text
	Каталог техники - catalog*
	Каталог техники.Товарные позиции - сatalog-items*
```


